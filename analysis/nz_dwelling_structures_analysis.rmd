---
title: "nz_dwelling_structures_analysis"
---

This notebook will load in data on dwelling strucures with age band configurations and create summaries/plots.
```{R}
library(tidyverse)
library(sf)
library(here)
library(viridis)
library(cowplot)
```
```{R}
SA2_TA_concordance <- read_csv(here("inputs","SA2_TA18_concordance.csv"))
```

```{R}
#load data
dwellings_sa2 <- read_csv(here("inputs","dwelling_composition_V2_sa2_safe - Checked.csv")) %>%
  #suppressed values coded as 999999, so replace with NA
  mutate(Count_dwellings = ifelse(Count_dwellings == 999999,NA,Count_dwellings),
         Count_people = ifelse(Count_people == 999999,NA,Count_people))

dwellings_nz <- read_csv(here("inputs","dwelling_composition_V2_nz_safe - Checked.csv")) %>%
  #suppressed values coded as 999999, so replace with NA
  mutate(Count_dwellings = ifelse(Count_dwellings == 999999,NA,Count_dwellings),
         Count_people = ifelse(Count_people == 999999,NA,Count_people))

```

```{R}
#how many configurations have full suppression of counts?
Count_suppressed <- nrow(dwellings_sa2 %>%
  filter(is.na(Count_dwellings)|is.na(Count_people))
  )
paste0(100*(Count_suppressed/nrow(dwellings_sa2)),"% counts fully suppressed")

sum(dwellings_sa2$Count_dwellings,na.rm = T)
#and what percent at each step of aggregation
aggregation_summary <- dwellings_sa2 %>% 
  group_by(Step) %>%
  summarise(`Dwellings (N)` = sum(Count_dwellings,na.rm = T),
            `People (N)` = sum(Count_people,na.rm = T)) %>%
  ungroup() %>%
  mutate(`Dwellings (%)` = round(`Dwellings (N)`/(sum(`Dwellings (N)`))*100,2),
         `People (%)` = round(`People (N)`/(sum(`People (N)`))*100,2)) %>%
  select(Step,`Dwellings (N)`,`Dwellings (%)`, `People (N)`,`People (%)`) %>%
  mutate(`Dwellings (N)` = scales::comma(`Dwellings (N)`),
         `People (N)` = scales::comma(`People (N)`))

print(xtable::xtable(aggregation_summary,align="llrrrr"),include.rownames = F)
```

```{R}
top10_configs <- dwellings_sa2 %>% 
  group_by(ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size,Config_adults,Config_workingAge) %>%
  summarise(`Dwellings (N)` = sum(Count_dwellings,na.rm = T),
            `People (N)` = sum(Count_people,na.rm = T)) %>%
  #arrange in order of popularity
  arrange(desc(`People (N)`)) %>%
  ungroup() %>%
  #remove "Config" from col names
  setNames(str_replace_all(names(.),pattern = "Config._?","")) %>%
  #calculate percentage coverage
  mutate(`Dwellings (%)` = round(`Dwellings (N)`/(sum(`Dwellings (N)`))*100,2),
         `People (%)` = round(`People (N)`/(sum(`People (N)`))*100,2)) %>%
  #tidy values for taable
  mutate_at(.vars=vars( `0to14`,`15to29`,`30to59`,`60plus`,
                        `Dwellings (N)`,`People (N)`),
            .funs = funs(scales::comma(.,accuracy = 1))
            ) %>%
  #select top 10
  head(10) %>%
  select(-c(dwelling_size,adults,workingAge)) %>% 
  select(`0to14`,`15to29`,`30to59`,`60plus`,
         `Dwellings (N)`, `Dwellings (%)`,`People (N)`, `People (%)`)

print(xtable::xtable(top10_configs,align="cccccrrrr"),include.rownames = F)

```



#Analysis of final node list
```{R}
#load data
concordance <- read_csv(here("inputs","SA2_TA18_concordance.csv"))

#load in node list
dwelling_nodelist <- read_csv(here("outputs","dwelling_sa2_nodelist_complete.csv"))

n_distinct(dwelling_nodelist$Dwelling_ID)
#and what percent at each step of aggregation
aggregation_summary <- dwelling_nodelist %>%
  mutate(Step = factor(Step,levels = c("Baseline","Step1","Step2","Step3","Step4","LargeDwellings"))) %>%
  group_by(Step) %>%
  summarise(`Dwellings (N)` = n_distinct(Dwelling_ID),
            `People (N)` = sum(dwelling_size)
            ) %>%
  ungroup() %>%
  mutate(`Dwellings (%)` = round(`Dwellings (N)`/(sum(`Dwellings (N)`))*100,2),
         `People (%)` = round(`People (N)`/(sum(`People (N)`))*100,2)) %>%
  select(Step,`Dwellings (N)`,`Dwellings (%)`, `People (N)`,`People (%)`) %>%
  mutate(`Dwellings (N)` = scales::comma(`Dwellings (N)`),
         `People (N)` = scales::comma(`People (N)`))

print(xtable::xtable(aggregation_summary,align="llrrrr"),include.rownames = F)
```

```{R}
dwelling_nodelist %>% 
  select(Dwelling_ID,matches("Config"), dwelling_size) %>%
  summarise(missing_0_14 = sum(is.na(ConfigA_0to14))/nrow(.),
            missing_15_29 = sum(is.na(ConfigB_15to29))/nrow(.),
            missing_30_59 = sum(is.na(ConfigC_30to59))/nrow(.),
            missing_60plus = sum(is.na(ConfigD_60plus))/nrow(.),
            missing_workingAge = sum(is.na(Config_workingAge))/nrow(.),
            missing_adults = sum(is.na(Config_adults))/nrow(.),
            missing_dwellingSize = sum(is.na(dwelling_size))/nrow(.))

```

```{R}
#compare distribution of nodelist
nodelist_distribution <- dwelling_nodelist %>%
  filter(Step != "LargeDwellings") %>%
  group_by(dwelling_size) %>% 
  summarise(Count_nodes = n_distinct(Dwelling_ID)) %>%
  ungroup() %>%
  filter(dwelling_size<=19) %>%
     mutate(size_grouped = case_when(dwelling_size == 0 ~ "Size 0",
                                    between(dwelling_size, 1,9) ~as.character(dwelling_size),
                                    between(dwelling_size, 10,14) ~"10-14",
                                    between(dwelling_size, 15,19) ~"15-19",
                                    TRUE ~ "NA")
         ) 

sa2_distribution <- dwellings_sa2 %>%
  group_by(dwelling_size) %>%
  summarise(`Dwellings (N)` = sum(Count_dwellings,na.rm = T)) %>%
  filter(!str_detect(dwelling_size,"or_more")) %>%
  mutate(dwelling_size = as.integer(dwelling_size))
  

nz_distribution <- dwellings_nz %>%
  filter(!str_detect(dwelling_size,"or_more")) %>%
  mutate(dwelling_size = as.integer(dwelling_size)) %>%
  group_by(dwelling_size) %>% 
  summarise(Count_nz = sum(Count_dwellings,na.rm = T)) %>%
  mutate(size_grouped = case_when(dwelling_size == 0 ~ "Size 0",
                                    between(dwelling_size, 1,9) ~as.character(dwelling_size),
                                    between(dwelling_size, 10,14) ~"10-14",
                                    between(dwelling_size, 15,19) ~"15-19",
                                    TRUE ~ "NA")
         ) 

ggplot() +
  geom_point(data = nodelist_distribution,
             aes(x = dwelling_size, y = Count_nodes,colour = "Generated", shape="Generated"),
             alpha = 0.5, size=2) +
  geom_point(data = nz_distribution,
             aes(x = dwelling_size, y = Count_nz,colour = "NZ-level",shape = "NZ-level"),
             alpha = 0.8, size= 2) + 
  geom_point(data = sa2_distribution,
             aes(x = dwelling_size, y = `Dwellings (N)`,shape = "SA2-level",color = "SA2-level"),
             stat="identity") + 
  theme_bw() +
  scale_colour_brewer(palette = "Set1") +
  scale_y_log10(labels=scales::comma_format(accuracy=1)) +
  scale_x_continuous(breaks = 1:19)+
  labs(x = "Dwelling Sizes",
       y = "Count Dwellings",
       shape = str_wrap("Geospatial Resolution",10),
       colour = str_wrap("Geospatial Resolution",10))

ggsave(here("outputs","Generated_dwellings_distribution_NZ_comparison.pdf"),dpi=600)
ggsave(here("outputs","Generated_dwellings_distribution_NZ_comparison.jpg"),dpi=600)

nodelist_distribution %>%
  group_by(size_grouped) %>%
              summarise(Count_nodes = sum(Count_nodes)) %>% ungroup() %>%
  left_join(nz_distribution %>% group_by(size_grouped) %>%
              summarise(Count_nz = sum(Count_nz)) %>% ungroup(), by ="size_grouped") %>%
  mutate(difference = (Count_nodes-Count_nz)/Count_nz)

```

```{R}
 dwelling_nodelist %>%
      group_by(dwelling_size) %>%
      summarise(Count = n_distinct(Dwelling_ID)) %>%
      ungroup() %>%
      ggplot(aes(x = dwelling_size, y = Count)) +
      geom_vline(xintercept = 20) +
      geom_point(alpha=0.2) +
      geom_text(aes(x = 30,y = 100000,label = str_wrap("Dwelling Size 20",8)),size=3)+ 
      scale_x_log10(labels=scales::comma_format(accuracy = 1)) + 
      scale_y_log10(labels=scales::comma_format(accuracy = 1)) + 
      theme_bw()  +
      labs(x="Dwelling Size", y= "Count") #+
      #ggtitle("Final Dwelling Size Distribution")

ggsave(here("outputs","complete_dwelling_size_distribution.pdf"),device = cairo_pdf)
ggsave(here("outputs","complete_dwelling_size_distribution.jpg"))

```

#compare number of dwellings at SA2 level with census 2018 counts
```{R}
#get count of dwellings per SA2 in nodelist
nodelist_dwellings_count <- dwelling_nodelist %>% 
  group_by(SA22018) %>%
  summarise(Imputed_Count = n_distinct(Dwelling_ID)) %>%
  ungroup() %>%
  mutate(prop_SA2_dwellings_imputed =Imputed_Count/sum(Imputed_Count) )


#get count of dwellings per SA2 from census
#complex-contagion/covid-19-sharedFiles/data/dwellings/from_nzDotStat
census_dwellings_count <-read_csv(here("inputs","NZStat_Census2018_SA2_OccupiedDwelling_Counts.csv")) %>%
  mutate(Count_occupied_dwellings = str_replace_all(Count_occupied_dwellings,"\\.\\.","0")) %>%
  mutate(Count_occupied_dwellings = as.integer(Count_occupied_dwellings)) %>%
  left_join(concordance, by = c("SA22018_N" = "SA22018__1")) %>%
  rename(SA22018 = "SA22018_V1",
         TA2018 = "TA2018_V1_") %>%
  mutate(prop_SA2_dwellings_census = Count_occupied_dwellings/sum(Count_occupied_dwellings))
  

combined_df <- nodelist_dwellings_count %>%
  left_join(census_dwellings_count, by = "SA22018")
```

# combined_df %>%
#   mutate(Imputed_census_diff = abs(prop_SA2_dwellings_imputed - prop_SA2_dwellings_census)) %>% 
#   ggplot(aes(x = prop_SA2_dwellings_imputed, y = prop_SA2_dwellings_census,colour=Imputed_census_diff)) +
#   geom_point() +
#   theme_bw() +
#   geom_abline(slope=1, intercept=0) +
#   coord_fixed(ratio = 1) +
#   scale_x_continuous(label = scales::percent) +
#   scale_y_continuous(label = scales::percent) +
#   viridis::scale_colour_viridis(label = scales::percent_format(accuracy=0.001),direction = 1) +
#   labs(colour = str_wrap("Absolute Difference Percentage Points",12),
#        x = "Generated SA2 Percentage",
#        y = "Census SA2 Percentage"
#        ) +
#   theme(legend.title = element_text(size=10), 
#         legend.spacing.y = unit(0.5, 'cm'))
# 
# ggsave(here("outputs","AbsoluteDiff_PercPoints_IDI_and_census_SA2_dwellings.pdf"), dpi=600)
# ggsave(here("outputs","AbsoluteDiff_PercPoints_IDI_and_census_SA2_dwellings.jpg"), dpi=600)


```{R}
combined_df %>%
  mutate(APD = abs((Imputed_Count-Count_occupied_dwellings)/Count_occupied_dwellings)) %>%
    filter(APD != Inf) %>%
  ggplot(aes(x = Imputed_Count, y = Count_occupied_dwellings,colour=APD)) +
  geom_point(alpha=0.5) +
  theme_bw() +
  geom_abline(slope=1, intercept=0) +
  coord_fixed(ratio = 1) +
  scale_x_continuous(label = scales::comma) +
  scale_y_continuous(label = scales::comma) +
  viridis::scale_colour_viridis(label = scales::percent_format(accuracy=1),direction = 1) +
  labs(colour = str_wrap("APD",12),
       x = "Generated SA2 Count",
       y = "Census SA2 Count"
       ) +
  theme(legend.title = element_text(size=10), 
        legend.spacing.y = unit(0.5, 'cm'))

ggsave(here("outputs","APD_IDI_and_census_SA2_dwellings.pdf"), dpi=600)
ggsave(here("outputs","APD_IDI_and_census_SA2_dwellings.jpg"), dpi=600)

combined_df %>%
  mutate(APD = abs((Imputed_Count-Count_occupied_dwellings)/Count_occupied_dwellings)) %>%
    filter(APD != Inf) %>%
  summary()

print(combined_df %>%
  mutate(APD = abs((Imputed_Count-Count_occupied_dwellings)/Count_occupied_dwellings)) %>%
  filter(APD != Inf) %>%
  arrange(desc(APD)) %>% 
    head(10) %>% 
    left_join(SA2_TA_concordance, by = c("SA22018" = "SA22018_V1")) %>%
    mutate_at(.vars = vars(SA22018, Imputed_Count, Count_occupied_dwellings),
              .funs = list(~as.integer(.))) %>%
    select(SA22018,SA22018__1,Imputed_Count,Count_occupied_dwellings, APD) %>%
  xtable::xtable(),row.names=F)
```
```{R}
cor.test(combined_df$Imputed_Count, combined_df$Count_occupied_dwellings, method="pearson")
cor.test(combined_df$Imputed_Count, combined_df$Count_occupied_dwellings, method="kendall")
cor.test(combined_df$Imputed_Count, combined_df$Count_occupied_dwellings, method="spearman")
```
```{R}
combined_df %>%
  mutate(SA22018 = as.character(SA22018)) %>%
  mutate(SA22018 = fct_reorder(.f = SA22018, .x = Count_occupied_dwellings)) %>% 
  mutate(Normalised_Diff = (Imputed_Count - Count_occupied_dwellings)/Count_occupied_dwellings) %>% #summary()#arrange(Normalised_Diff)
  ggplot(aes(x = SA22018, y = Normalised_Diff, colour = Count_occupied_dwellings)) +
  geom_point(stat="identity",alpha=0.3) +
  geom_hline(yintercept = 0) +
  theme_bw() +
  viridis::scale_color_viridis(trans = "log10", labels = scales::comma_format(accuracy = 1)) +
  theme(axis.text.x = element_blank(),panel.grid = element_blank()
          ) + 
  scale_y_continuous(labels = scales::percent_format(accuracy = 1)) +
  labs(y = "Percentage Difference",
       x = "SA2 (ordered according to number of dwellings in census data)",
       colour = str_wrap("Number of dwellings (Census)",12),
       #caption = "Excluding outlier > 500%"
       )+
  ggtitle("Difference in number of generated dwellings compared to census") 

ggsave(here("outputs","Residuals_IDI_and_census_SA2_dwellings.pdf"), dpi=600)
ggsave(here("outputs","Residuals_IDI_and_census_SA2_dwellings.jpg"), dpi=600)

```

```{R}
combined_df %>%
  mutate(DwellingsDiff = abs(Imputed_Count-Count_occupied_dwellings)) %>% 
  ggplot(aes(x = Imputed_Count, y = Count_occupied_dwellings,colour=DwellingsDiff)) +
  geom_point() +
  theme_bw() +
  geom_abline(slope=1, intercept=0) +
  coord_fixed(ratio = 1) +
  scale_x_continuous(label = scales::comma) +
  scale_y_continuous(label = scales::comma) +
  viridis::scale_colour_viridis(label = scales::comma_format(accuracy=1),direction = 1) +
  labs(colour = str_wrap("Dwellings Difference",12),
       x = "Generated SA2 Count",
       y = "Census SA2 Count"
       ) +
  theme(legend.title = element_text(size=10), 
        legend.spacing.y = unit(0.5, 'cm'))

ggsave(here("outputs","Dwellings_diff_IDI_and_census_SA2_dwellings.pdf"), dpi=600)
ggsave(here("outputs","Dwellings_diff_IDI_and_census_SA2_dwellings.jpg"), dpi=600)
```

##Map spatial distribution
```{R}
#load in shape files for mapping
SA2_sf <- read_sf(here("inputs","shape_files","SA2_2018_simplify.shp")) %>%
  #change REGC2018_V to numeric
  mutate(SA22018 = as.numeric(SA22018)) %>%
  #exclude chathams
  filter(REGC2018_N != "Area Outside Region")
  

nz_SA2_sf <- SA2_sf %>%
  left_join(
    dwelling_nodelist %>% 
      group_by(SA22018) %>%
      summarise(Dwellings = n_distinct(Dwelling_ID)) %>%
      ungroup(),
    by = "SA22018")

min_Dwellings <- min(nz_SA2_sf$Dwellings,na.rm = T)
max_Dwellings <- max(nz_SA2_sf$Dwellings, na.rm = T)
my_breaks <- c(10,100,1000,10000)

#filter by locations
auckland_sf <- nz_SA2_sf %>%
   st_crop(y = c(xmin=1730000,
                ymin=5900000,
                xmax=1780000,
                ymax=5940000))
  #filter(TA2018 == "076")
hamilton_sf <- nz_SA2_sf %>%
   st_crop(y = c(xmin=1785000,
                ymin=5805000,
                xmax=1820000,
                ymax=5830000)) 
  #filter(TA2018 == "016")
wellington_sf <- nz_SA2_sf %>%
   st_crop(y = c(xmin=1735000,
                ymin=5300000,
                xmax=1780000,
                ymax=5450000))
  #filter(TA2018 == "047")
christchurch_sf <- nz_SA2_sf %>%
   st_crop(y = c(xmin=1550000,
                ymin=5160000,
                xmax=1590000,
                ymax=5190000)) 
  #filter(TA2018 == "060")
dunedin_sf <- nz_SA2_sf %>%
   st_crop(y =c(xmin=1380000,
                ymin=4900000,
                xmax=1450000,
                ymax=4940000)) 
  #filter(TA2018 == "071")

#get regional borders
auckland_border <- st_union(auckland_sf$geometry)
hamilton_border <- st_union(hamilton_sf$geometry)
wellington_border <- st_union(wellington_sf$geometry)
christchurch_border <- st_union(christchurch_sf$geometry)
dunedin_border <- st_union(dunedin_sf$geometry)

#get NZ level map
NZ_plot <- nz_SA2_sf %>%
  ggplot() +
  geom_sf(aes(geometry = geometry,
              fill = Dwellings),
          color = NA) +
  theme_void() +
  geom_sf(data = auckland_border, fill = NA, color = "black", size = 0.1) +
  geom_sf(data = hamilton_border, fill = NA, color = "black", size = 0.1) +
  geom_sf(data = wellington_border, fill = NA, color = "black", size = 0.1) +
  geom_sf(data = christchurch_border, fill = NA, color = "black", size = 0.1) +
  geom_sf(data = dunedin_border, fill = NA, color = "black", size = 0.1) +
  scale_fill_gradientn(colours=viridis(6), name = "Dwellings", labels = scales::comma,
                       #trans = "log",breaks=my_breaks,
                        na.value = "white", limits = c(min_Dwellings,max_Dwellings)) +
  guides(fill = guide_colourbar(title.position="top")) +
  theme(legend.position = "bottom",
        legend.title.align = 1,
        legend.text = element_text(angle=45,hjust=1))
#get a copy of the legend
legend <- get_legend(NZ_plot)
#remove the legend from the plot
  NZ_plot <- NZ_plot + theme(legend.position = "none")

#get maps for different regions
auckland_plot <- ggplot(data = auckland_sf) +
  geom_sf(aes(geometry = geometry,
              fill = Dwellings),
          color = adjustcolor("black",alpha.f = 0.001)) +
  theme_void() +
  ggtitle("Auckland") + 
  scale_fill_gradientn(colours=viridis(6), name = "Dwellings",
                       #trans = "log",breaks=my_breaks,
                       na.value = "white", limits = c(min_Dwellings,max_Dwellings)
                       ) +
  theme(legend.position = "none",
        panel.border = element_rect(colour = "black", fill=NA, size=0.8),
        plot.title = element_text(size = 10)
        ) 

hamilton_plot <- ggplot(data = hamilton_sf) +
  geom_sf(aes(geometry = geometry,
              fill = Dwellings),
          color = adjustcolor("black",alpha.f = 0.001)) +
  theme_void() +
  ggtitle("Hamilton") + 
scale_fill_gradientn(colours=viridis(6), name = "Dwellings",
                     #trans = "log",breaks=my_breaks,
                     na.value = "white", limits = c(min_Dwellings,max_Dwellings)
                     ) +
  theme(legend.position = "none",
        panel.border = element_rect(colour = "black", fill=NA, size=0.8),
        plot.title = element_text(hjust = 1, size = 10)
        ) 

wellington_plot <- ggplot(data = wellington_sf) +
  geom_sf(aes(geometry = geometry,
              fill = Dwellings),
          color = adjustcolor("black",alpha.f = 0.001)) +
  theme_void() +
  ggtitle("Wellington") + 
scale_fill_gradientn(colours=viridis(6), name = "Dwellings",
                     #trans = "log",breaks=my_breaks,
                        na.value = "white", limits = c(min_Dwellings,max_Dwellings)
                     ) + 
  theme(legend.position = "none",
        panel.border = element_rect(colour = "black", fill=NA, size=0.8),
        plot.title = element_text(size = 10)
        ) 

christchurch_plot <- ggplot(data = christchurch_sf) +
  geom_sf(aes(geometry = geometry,
              fill = Dwellings),
          color = adjustcolor("black",alpha.f = 0.001)) +
  theme_void() +
  ggtitle("Christchurch") + 
scale_fill_gradientn(colours=viridis(6), name = "Dwellings",
                     #trans = "log",breaks=my_breaks,
                        na.value = "white", limits = c(min_Dwellings,max_Dwellings)
                     ) +
  theme(legend.position = "none",
        panel.border = element_rect(colour = "black", fill=NA, size=0.8),
        plot.title = element_text(hjust = 1,size = 10)
        ) 

dunedin_plot <- ggplot(data = dunedin_sf) +
  geom_sf(aes(geometry = geometry,
              fill = Dwellings),
          color = adjustcolor("black",alpha.f = 0.001)) +
  theme_void() +
  ggtitle("Dunedin") + 
scale_fill_gradientn(colours=viridis(6), name = "Dwellings",
                     #trans = "log",breaks=my_breaks,
                        na.value = "white", limits = c(min_Dwellings,max_Dwellings)
                     ) +
  theme(legend.position = "none",
        panel.border = element_rect(colour = "black", fill=NA, size=0.8),
        plot.title = element_text(size = 10)
        ) 

#combine maps
ggdraw() +
  draw_plot(NZ_plot) +
  draw_plot(auckland_plot, x = 0.70, y = 0.65, width = 0.3, height = 0.3) +
  draw_plot(hamilton_plot, x = 0.036, y = 0.65, width = 0.3, height = 0.3) +
  draw_plot(christchurch_plot, x = 0.02
            , y = 0.35, width = 0.3, height = 0.3) +
  draw_plot(wellington_plot, x = 0.713, y = 0.35, width = 0.3, height = 0.3) +
  draw_plot(dunedin_plot, x = 0.705, y = 0.05, width = 0.3, height = 0.3) +
  draw_plot(legend, x = 0.02, y = 0.05, width = 0.3, height = 0.3) +

  #draw lines to connect areas to main NZ map
  #auckland
  draw_line(x = c(0.5675, 0.765),
            y = c(0.775, 0.775),
            color = "black", size = 0.5) +
  #hamilton
  draw_line(x = c(0.27, 0.58),
            y = c(0.715, 0.715),
            color = "black", size = 0.5) +
  #Christchurch
  draw_line(x = c(0.25, 0.49),
            y = c(0.47, 0.32),
            color = "black", size = 0.5) +
  #wellington
  draw_line(x = c(0.57, 0.78),
            y = c(0.47, 0.47),
            color = "black", size = 0.5) +
  #Dunedin
  draw_line(x = c(0.43, 0.77),
            y = c(0.16, 0.15),
            color = "black", size = 0.5) +
  draw_label(x = 0.55,y=0.98,size =11.25,"Spatial Distribution of NZ Dwellings")

ggsave(here("outputs","NZ_Dwelling_Distribution_map.pdf"))
ggsave(here("outputs","NZ_Dwelling_Distribution_map.jpg"),dpi=600)
```