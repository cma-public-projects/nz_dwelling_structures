
#This code adds geospatial location to aged care facility data obtained from 
# https://www.health.govt.nz/your-health/certified-providers/aged-care
#Will also need to restiger with google API and use a key to scrape location data from google maps. 
agedCare_addSpatial <- function(aged_care_facilities, google_API_key,SA2_sf,concordance){
  require(tidyverse)
  # library(ggmap)
  require(here)
  
  
  #clean columns
  aged_care_facilities_clean <- aged_care_facilities %>%
    #tidy variable names
    rename(Name = "Premises Name",Certification="Certification Service Type",
           Service_type = "Service Types",Total_beds = "Total Beds",
           Postcode = "Premises Address Post Code",DHB = "DHB Name") %>%
    #if a line in the address is NA, replace with empty string
    mutate(`Premises Address` = ifelse(is.na(`Premises Address`),"",`Premises Address`),
           `Premises Address Suburb/Road` = ifelse(is.na(`Premises Address Suburb/Road`),"",`Premises Address Suburb/Road`),
           `Premises Address Town/City` = ifelse(is.na(`Premises Address Town/City`),"",`Premises Address Town/City`),
           Postcode = ifelse(is.na(Postcode),"",Postcode)) %>%
    #put address line together into one string
    mutate(Address = paste0(`Premises Address`,", ",
                            `Premises Address Suburb/Road`,", ",
                            `Premises Address Town/City`,", ",
                            Postcode, ", NZ")
    ) %>%
    #tidy strings where missing lines occur
    mutate(Address = str_replace_all(Address," ,","")) %>%
    select(Name,Address,Certification,Service_type,Total_beds,Postcode, DHB)
  
  #google API
  register_google(key = google_API_key)
  
  aged_care_facilities_clean_new <- aged_care_facilities_clean
  
  for(i in 1:nrow(aged_care_facilities_clean)){
    #add delay
    date_time<-Sys.time()
    while((as.numeric(Sys.time()) - as.numeric(date_time))<10){} #dummy while loop
    result <- geocode(aged_care_facilities_clean_new$Address[i],
                      output = "latlona",
                      source = "google")
    
    aged_care_facilities_clean_new$lon[i] <- as.numeric(result[1])
    aged_care_facilities_clean_new$lat[i] <- as.numeric(result[2])
    aged_care_facilities_clean_new$geoAddress[i] <- as.character(result[3])
    # write_csv(aged_care_facilities_clean_new, here("outputs","age_care_clean_so_far.csv"))
    
  }
  
  #aged_care_facilities_clean_done <- aged_care_facilities_clean_new
  #aged_care_facilities_clean_new %>% filter(row_number()>=454)
  # Add ID
  aged_care_facilities_complete <- aged_care_facilities_clean_new %>%
    mutate(Care_ID = row_number()) %>%
    select(Care_ID,everything())
  
  library(sf)
  # SA2_sf <- read_sf(here("inputs","Shapefiles","SA2_2018_Land.shp"))
  # SA2_concordance <- read_csv(here("inputs","SA2_TA18_concordance.csv")) %>%
  #   rename(SA22018 ="SA22018_V1",
  #          SA22018_N = "SA22018__1",
  #          TA2018 = "TA2018_V1_",
  #          TA2018_N = "TA2018_V_1")
  
  care_points <- aged_care_facilities_complete %>%
    select(Care_ID, Name,Certification,Service_type,Total_beds, lon, lat) %>%
    filter(!is.na(lon))
  
  care_points <- st_as_sf(care_points, coords = c("lon","lat"), crs = 4326) 
  care_NZTM <- st_transform(care_points,2193)
  
  ### get overlaps/contains/intersects (should all be the same...)
  care_locations <- st_join(care_NZTM,SA2_sf, join = st_within)
  
  care_locations_full <- care_locations %>%
    select(Care_ID,SA22018) %>%
    st_set_geometry(NULL) %>%
    left_join(concordance %>% mutate(SA22018 = as.character(SA22018)), by = "SA22018") 
  
  
  care_fulldata <- aged_care_facilities_complete %>%
    left_join(care_locations_full, by = "Care_ID")
  
  #check there are no duplicated centres
  care_fulldata$Care_ID %>% duplicated() %>% sum()
  
  #final tidy before saving
  care_complete <- care_fulldata %>%
    select(Care_ID,SA22018,SA22018_N,TA2018,TA2018_N,Name,Total_beds,Certification,Service_type,
           Postcode,DHB,lon,lat,Address,geoAddress) %>%
    mutate(Care_ID = paste0("AC",
                            str_pad(string = TA2018,width = 2,side = "left",pad = "0"),
                            str_pad(string = Care_ID,width = 4,side = "left",pad = "0")
    ))
  
  # save complete data
  # write_csv(care_complete,here("outputs",paste0(Sys.Date(),"-AgedCareFacilities_clean.csv")))
  # write_csv(care_complete,here("outputs",paste0("AgedCareFacilities_clean.csv")))
  
  return(care_complete)
}