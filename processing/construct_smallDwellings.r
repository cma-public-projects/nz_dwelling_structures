#This notebook will build the dwelling node list, using data from the IDI. The input data can be found in the Dropbox
#covid-19-sharedFiles\data\IDI_raw_outputs\Dwelling_structure. Note: This data only includes `dwelling_size < 20
#To run this R script, copy the Dwelling_structure folder to the gitlab repository nz_dwelling_structures\inputs.
#Always make sure to open the .Rproj file located in nz_dwelling_structures prior to running, 
#as this will make the `here()` library point to the correct directories when loading inputs and writing outputs. 

construct_smallDwellings <- function(SEED = 123, dwelling_sa2, dwellings_ta, dwellings_nz, concordance){
  # libraries
  require(here)
  require(tidyverse)
  require(splitstackshape)
  
  # dwellings_sa2 <- read_csv(here("inputs", "dwelling_composition_V2_sa2_safe - Checked.csv"))
  # dwellings_ta <- read_csv(here("inputs", "dwelling_composition_V2_ta_safe - Checked.csv")) %>%
  #   filter(ur_ta != 999)
  # dwellings_nz <- read_csv(here("inputs", "dwelling_composition_V2_nz_safe - Checked.csv"))
  
  # load concordance
  #can be found `Dropbox (Complex systems)\covid-19-sharedFiles\data\spatialdata\concordances
  # this data needs to be copied from Dropbox/data/spatial and Dropbox/data/IDI_raw_outputs into git-repo/RCode/inputs
  ## these data files should be in
  
  # #concordance
  # concordance_names <-read_csv(here("inputs","SA2_TA18_REGC_concordance.csv"),
  #                              col_types = cols(
  #                                SA22018 = col_double(),
  #                                SA22018_N = col_character(),
  #                                REGC2018 = col_double(),
  #                                REGC2018_N = col_character(),
  #                                TA2018 = col_double(),
  #                                TA2018_N = col_character(),
  #                                SA2_area = col_double()
  #                              )
  # ) %>%
  #   select(starts_with('SA22018'),starts_with("TA2018")) %>%
  #   unique()
  # 
  # 
  # concordance <- read_csv(here("inputs","MB18_SA2_majority_concordance_IDI.csv"),
  #                         col_types = cols(
  #                           MB2018 = col_double(),
  #                           SA22018 = col_double(),
  #                           REGC2018 = col_double(),
  #                           TA2018 = col_double(),
  #                           DHB2015 = col_double(),
  #                           DHBCON2015 = col_double()
  #                         )
  # ) %>%
  #   select(SA22018,TA2018) %>%
  #   unique() %>%
  #   left_join(concordance_names %>% select(SA22018,SA22018_N), by = "SA22018") %>%
  #   left_join(concordance_names %>% select(TA2018,TA2018_N) %>% unique(), by = "TA2018") %>%
  #   select(SA22018,SA22018_N,TA2018,TA2018_N)
  

  
  #Begin by saving cleaned copies of the NZ level data. 
  #this involves replacing missing counts of dwellings with a random number from 1-5,
  
  dwellings_nz_clean <- dwellings_nz %>% 
    rowwise() %>%
    #if count_dwellings is missing (999999), then fill in with random number from 1 to 5
    mutate(Count_dwellings = ifelse(Count_dwellings == "999999", sample(1:5),Count_dwellings)) %>%
    #get the number of people by multiplying the dwelling size by the count of those dwellings.
    mutate(Count_people = ifelse(Count_people == "999999", parse_number(x =dwelling_size)*Count_dwellings, Count_people)) %>%
    ungroup() 
  
  #write_csv(dwellings_nz_clean,here("outputs","dwelling_composition_V2_nz_safe_cleaned.csv"))

  
  # impute missing TA-level dwellings based on NZ level counts
  dwellings_ta_clean <- dwellings_ta %>% 
    rowwise() %>%
    #if count_dwellings is missing (999999), then fill in with random number from 1 to 5
    mutate(Count_dwellings = ifelse(Count_dwellings == "999999", sample(1:5),Count_dwellings)) %>%
    #get the number of people by multiplying the dwelling size by the count of those dwellings.
    mutate(Count_people = ifelse(Count_people == "999999", parse_number(x =dwelling_size)*Count_dwellings, Count_people)) %>%
    ungroup() %>%
    mutate(TA2018 = str_pad(ur_ta, width = 2, side = "left", pad=0)) %>% 
    filter(TA2018 != 999) %>% select(TA2018,everything(),-ur_ta)
  
  #identify set of dwellings that are not present in ta level dwelling data (with suppression)
  Missing_TA_dwellings_from_NZ <- dwellings_ta %>%
    mutate(Count_dwellings= ifelse(Count_dwellings==999999, NA,Count_dwellings),
           Count_people = ifelse(Count_people==999999, NA,Count_people)) %>%
    filter(Step != "Step4") %>%
    group_by(dwelling_size) %>%
    summarise(Count_dwellings_TA= sum(Count_dwellings)) %>%
    ungroup() %>% 
    left_join(dwellings_nz_clean %>% 
                group_by(dwelling_size) %>%
                summarise(Count_dwellings_NZ= sum(Count_dwellings)) %>%
                ungroup(),
              by = "dwelling_size") %>%
    mutate(Difference_dwellings = Count_dwellings_NZ-Count_dwellings_TA) %>%
    select(dwelling_size, Difference_dwellings) 
  
  #for TA-level data, identify dwelling configurations that are missing info on dwelling size (Step 4)
  step4_dwellings_TA <- dwellings_ta_clean %>% 
    filter(Step == "Step4") %>%
    arrange(desc(parse_number(dwelling_size)))
  
  #get the pool of dwellings sizes that can be drawn on to impute missing sizes
  Missing_TA_dwellings_pool <- Missing_TA_dwellings_from_NZ %>%
    splitstackshape::expandRows("Difference_dwellings") %>%
    #create unique identifier for each "Dwelling" %>%
    mutate(dwelling_identifier = paste0("D",row_number()))
  
  
  # data frame to save imputed step 4 dwellings
  imputed_step_4_dwellings <- data.frame()
  
  #for TA in step4 dwellings (arranged in descending order of potential dwelling size)
  for(TA in unique(step4_dwellings_TA$TA2018)){
    
    #get individual dwellings for that TA
    target_dwellings <- step4_dwellings_TA %>%
      filter(TA2018 == TA) %>%
      splitstackshape::expandRows("Count_dwellings")
    
    #print the TA and the min size of dwellings to impute within that TA 
    print(paste0(TA," - min size = ", first(parse_number(target_dwellings$dwelling_size))))
    
    #get the number of dwellings that need to be assigned a size
    number_to_sample <- nrow(target_dwellings)
    
    # get a suitable pool of dwellings that can be assigned to step4 dwellings in that TA
    suitable_dwellings_to_sample <- Missing_TA_dwellings_pool %>%
      #suitable dwellings are within the specific dwelling size range provided
      filter(as.integer(dwelling_size) >= first(parse_number(target_dwellings$dwelling_size)))
    
    #if the number of suitable dwellings to sample is not suffient to match the number of rows that need imputation, 
    #we will just add n candidates of the minimum size to the pool
    if(nrow(suitable_dwellings_to_sample)<number_to_sample){
      #number to add 
      n_needed<- number_to_sample - nrow(suitable_dwellings_to_sample)
      #get the min size
      min_size <-  first(parse_number(target_dwellings$dwelling_size))
      #add new candidates to what is available to make up the numbers
      suitable_dwellings_to_sample <- suitable_dwellings_to_sample %>%
        bind_rows(data.frame(dwelling_size = as.character(rep(min_size,n_needed)),
                             dwelling_identifier = "NA"))
      warning(paste0("Had to add ", n_needed, " dwellings of min size ", min_size, " to impute in TA ", TA))}
    
    #sample dwellings
    sample_dwelling_sizes <- sample(x = 1:nrow(suitable_dwellings_to_sample),
                                    size=number_to_sample,
                                    replace = F)
    
    #get the resulting vector of dwelling sizes
    result_row <- suitable_dwellings_to_sample[sample_dwelling_sizes,] 
    
    #remove the selected dwellings from future sample pools
    Missing_TA_dwellings_pool <- Missing_TA_dwellings_pool %>%
      filter(!(dwelling_identifier %in% result_row$dwelling_identifier))
    
    #replace original dwelling size string with the imputed values
    target_dwellings$dwelling_size <- result_row$dwelling_size
    
    target_dwellings <- target_dwellings %>%
      group_by_all() %>%
      summarise(Count_dwellings = n(),
                Count_people = Count_dwellings*as.integer(dwelling_size),
                .groups = "drop") %>%
      ungroup() %>%
      unique()
    #save row with new imputed
    imputed_step_4_dwellings <- bind_rows(imputed_step_4_dwellings, target_dwellings)
  }
  
  
  dwellings_ta_imputed <- dwellings_ta_clean %>%
    #remove original step 4 dwellings
    filter(Step!="Step4") %>%
    #add new imputed dwellings
    bind_rows(imputed_step_4_dwellings)
  
  
  #write_csv(dwellings_ta_imputed,here("outputs","dwelling_composition_V2_ta_safe_cleaned.csv"))

  
  #identify set of dwellings that are not present in ta level dwelling data (with suppression)
  print(
    dwellings_ta_imputed %>%
      group_by(dwelling_size) %>%
      summarise(Count_dwellings_TA= sum(Count_dwellings)) %>%
      ungroup() %>% 
      left_join(dwellings_nz_clean %>% 
                  group_by(dwelling_size) %>%
                  summarise(Count_dwellings_NZ= sum(Count_dwellings)) %>%
                  ungroup(),
                by = "dwelling_size") %>%
      mutate(Difference_dwellings = Count_dwellings_NZ-Count_dwellings_TA)
  )

  
  # impute missing SA2-level dwellings based on imputed TA counts
  dwellings_sa2_clean <- dwellings_sa2 %>% 
    rowwise() %>%
    #if count_dwellings is missing (999999), then fill in with random number from 1 to 5
    mutate(Count_dwellings = ifelse(Count_dwellings == "999999", sample(1:5),Count_dwellings)) %>%
    #get the number of people by multiplying the dwelling size by the count of those dwellings.
    mutate(Count_people = ifelse(Count_people == "999999", parse_number(x =dwelling_size)*Count_dwellings, Count_people)) %>%
    ungroup() %>%
    #join concordance an remove TA 999 
    left_join(concordance %>% select(SA22018,TA2018) %>% unique, by = c("ur_sa2" = "SA22018")) %>%
    mutate(TA2018 = str_pad(TA2018, width = 2, side = "left", pad=0)) %>% 
    filter(TA2018 != 999) %>% select(ur_sa2,TA2018, everything())
  
  
  #identify set of dwellings that are not present in ta level dwelling data (with suppression)
  Missing_SA2_dwellings_from_TA <- dwellings_ta_imputed %>% 
    group_by(TA2018,dwelling_size) %>%
    summarise(Count_dwellings_TA= sum(Count_dwellings)) %>%
    ungroup() %>%
    left_join(dwellings_sa2 %>%
                left_join(concordance %>% 
                            select(SA22018,TA2018) %>%
                            mutate(TA2018 = str_pad(string = TA2018,side="left",pad="0",width=2)),
                          by = c("ur_sa2"="SA22018")) %>%
                mutate(Count_dwellings= ifelse(Count_dwellings==999999, NA,Count_dwellings),
                       Count_people = ifelse(Count_people==999999, NA,Count_people)) %>%
                #only keep those dwelling we know the size (exclude step 4 dwellings)
                filter(Step != "Step4") %>%
                #aggregate count of dwellings for each dwelling size at TA level
                group_by(TA2018, dwelling_size) %>%
                summarise(Count_dwellings_SA2= sum(Count_dwellings)) %>%
                ungroup(),
              by = c("dwelling_size","TA2018")) %>%
    #if there are no dwellings of a size in the sa2 level data, then we replace with 0
    mutate(Count_dwellings_SA2 = ifelse(is.na(Count_dwellings_SA2),0,Count_dwellings_SA2)) %>%
    mutate(Difference_dwellings = Count_dwellings_TA-Count_dwellings_SA2) %>%
    select(TA2018,dwelling_size, Difference_dwellings) 
  
  #for sa2-level data, identify dwelling configurations that are missing info on dwelling size (Step 4)
  step4_dwellings_SA2 <- dwellings_sa2_clean %>% 
    filter(Step == "Step4") %>%
    group_by(TA2018) %>%
    arrange(desc(parse_number(dwelling_size)),.by_group = T) %>%
    ungroup()
  
  #get the pool of dwellings sizes that can be drawn on to impute missing sizes
  Missing_SA2_dwellings_pool <- Missing_SA2_dwellings_from_TA %>%
    #remove any negative difference 
    filter(Difference_dwellings>0) %>%
    splitstackshape::expandRows("Difference_dwellings") %>%
    #create unique idenfier for each "Dwelling" %>%
    mutate(dwelling_identifier = paste0("D",row_number()))
  
  
  # data frame to save imputed step 4 dwellings
  imputed_step_4_dwellings <- data.frame()
  
  #for TA in step4 dwellings (arranged in descending order of potential dwelling size)
  for(SA2 in unique(step4_dwellings_SA2$ur_sa2)){
    
    #get individual dwellings for that TA
    target_dwellings <- step4_dwellings_SA2 %>%
      filter(ur_sa2 == SA2) %>%
      splitstackshape::expandRows("Count_dwellings")
    
    #print the TA that we are starting to impute SA2 dwellings for
    if(!(first(target_dwellings$TA2018) %in% imputed_step_4_dwellings$TA2018)){
      print(paste0("Started imputing SA2-level dwellings in TA ", first(target_dwellings$TA2018)))
      }
    
    #get the number of dwellings that need to be assigned a size
    number_to_sample <- nrow(target_dwellings)
    
    # get a suitable pool of dwellings that can be assigned to step4 dwellings in that SA2
    # suitable dwellings need to come from the same TA
    suitable_dwellings_to_sample <- Missing_SA2_dwellings_pool %>%
      filter(TA2018 == first(target_dwellings$TA2018)) %>%
      #suitable dwellings are within the specific dwelling size range provided
      filter(as.integer(dwelling_size) >= first(parse_number(target_dwellings$dwelling_size)))
    
    #if the number of suitable dwellings to sample is not suffient to match the number of rows that need imputation, 
    #we will just add n candidates of the minimum size to the pool
    if(nrow(suitable_dwellings_to_sample)<number_to_sample){
      #number to add 
      n_needed<- number_to_sample - nrow(suitable_dwellings_to_sample)
      #get the min size
      min_size <-  first(parse_number(target_dwellings$dwelling_size))
      #add new candidates to what is available to make up the numbers
      suitable_dwellings_to_sample <- suitable_dwellings_to_sample %>%
        bind_rows(data.frame(dwelling_size = as.character(rep(min_size,n_needed)),
                             dwelling_identifier = "NA"))
      #warning(paste0("Had to add ", n_needed, " dwellings of min size ", min_size, " to impute in SA2 ", SA2))
      }
    
    #sample dwellings
    sample_dwelling_sizes <- sample(x = 1:nrow(suitable_dwellings_to_sample),
                                    size=number_to_sample,
                                    replace = F)
    
    #get the resulting vector of dwelling sizes
    result_row <- suitable_dwellings_to_sample[sample_dwelling_sizes,] 
    
    #remove the selected dwellings from future sample pools
    Missing_SA2_dwellings_pool <- Missing_SA2_dwellings_pool %>%
      filter(!(dwelling_identifier %in% result_row$dwelling_identifier))
    
    #replace original dwelling size string with the imputed values
    target_dwellings$dwelling_size <- result_row$dwelling_size
    
    target_dwellings <- target_dwellings %>%
      group_by_all() %>%
      summarise(Count_dwellings = n(),
                Count_people = Count_dwellings*as.integer(dwelling_size),
                .groups = "drop") %>%
      ungroup() %>%
      unique()
    #save row with new imputed
    imputed_step_4_dwellings <- bind_rows(imputed_step_4_dwellings, target_dwellings)
  }
  
  
  dwellings_sa2_imputed <- dwellings_sa2_clean %>%
    #remove original step 4 dwellings
    filter(Step!="Step4") %>%
    #add new imputed dwellings
    bind_rows(imputed_step_4_dwellings)
  

  #write_csv(dwellings_sa2_imputed,here("outputs","dwelling_composition_V2_sa2_safe_cleaned.csv"))

  #identify set of dwellings that are not present in ta level dwelling data (with suppression)
  # print(
  #   dwellings_sa2_imputed %>%
  #     group_by(dwelling_size) %>%
  #     summarise(Count_dwellings_SA2= sum(Count_dwellings)) %>%
  #     ungroup() %>% 
  #     left_join(dwellings_nz_clean %>% 
  #                 group_by(dwelling_size) %>%
  #                 summarise(Count_dwellings_NZ= sum(Count_dwellings)) %>%
  #                 ungroup(),
  #               by = "dwelling_size") %>%
  #     mutate(Difference_dwellings = Count_dwellings_NZ-Count_dwellings_SA2)
  # )
  
  # create final SA2 level node list 
  #expand those we already know (Baseline-Step3)
  dwellings_sa2_nodelist <- expandRows(dwellings_sa2_imputed, count = "Count_dwellings")
  
  if(nrow(dwellings_sa2_nodelist %>% filter(dwelling_size<1))){
    warning("before distribution allocation function ",nrow(dwellings_sa2_nodelist %>% filter(dwelling_size<1)),
            " dwellings of size 0 were produced.")
    
  }
  
  
  #tidy and save output
  dwellings_sa2_nodelist_complete <- dwellings_sa2_nodelist %>%
    #change dwelling size data type to integer
    mutate(dwelling_size = as.integer(dwelling_size)
    ) %>%
    ungroup() 
  
    if(nrow(dwellings_sa2_nodelist_complete %>% filter(dwelling_size<1))){
      warning(nrow(dwellings_sa2_nodelist_complete %>% filter(dwelling_size<1)),
              " dwellings of size 0 were produced.")
      
    }
    
  #clean for output and add ID column
    dwellings_sa2_nodelist_complete <- dwellings_sa2_nodelist_complete %>%
    #remove dwellings of size 0
    filter(dwelling_size!=0) %>%
    #tidy
    rename(SA22018 = "ur_sa2") %>%
    group_by(TA2018) %>%
    #create dwelling ID (these IDs may get recast later on when the network is built)
    mutate(Dwelling_ID = paste0("DWS",
                                str_pad(TA2018,width=2,side="left",pad="0"),
                                str_pad(row_number(), width = 7, side="left", pad = "0"))) %>%
    ungroup() %>%
    select(Dwelling_ID,SA22018, TA2018, everything(),-Count_people)
  
  #compare generated distribution to NZ level
  nz_dwellings <- dwellings_nz %>%
    #filter(parse_number(dwelling_size)>12) %>%
    group_by(dwelling_size) %>%
    summarise(Count_dwellings = sum(Count_dwellings,na.rm = T)) %>%
    filter(!str_detect(dwelling_size,"or_more")) %>%
    mutate(dwelling_size = as.integer(dwelling_size)) 
  
  new_generated_dwellings <- dwellings_sa2_nodelist_complete %>% 
    group_by(dwelling_size) %>%
    summarise(Count_dwellings = n_distinct(Dwelling_ID)) 

  
  #plot  
  print(ggplot() +
          geom_point(data = nz_dwellings, aes(x = dwelling_size, y = Count_dwellings, colour = "NZ"),stat="identity") +  
          #geom_point(data = old_generated_dwellings, aes(x = dwelling_size, y = Count_dwellings, colour = "Previous Method"),stat="identity") + 
          geom_point(data = new_generated_dwellings, aes(x = dwelling_size, y = Count_dwellings, colour = "Imputed SA2"),stat="identity") + 
          scale_x_continuous(breaks = 1:19, limits= c(0,19)) +
          scale_y_continuous(trans="log10",labels = scales::comma_format(accuracy = 1)) +
          labs(x = "Dwelling Size", y = "Count", colour = "Data") +
          theme_bw()+
          ggtitle("Generated dwellings vs NZ small dwelling size distribution")
  )
  
  #save output
  #write_csv(dwellings_sa2_nodelist_complete, here("outputs", "smallDwellings_sa2_nodelist.csv"))
  #write_csv(dwellings_sa2_nodelist_complete, here("outputs", paste0(Sys.Date(),"-smallDwellings_sa2_nodelist.csv")))
  
  #return final node list
  return(dwellings_sa2_nodelist_complete)
}
  

