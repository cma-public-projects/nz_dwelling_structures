
#This script will build the dwelling node list, using data from the IDI. The input data can be found in the Dropbox
#covid-19-sharedFiles\data\IDI_raw_outputs\Dwelling_structure. Note: This data only includes `dwelling_size < 20`
#To run this R notebook, copy the Dwelling_structure folder to the gitlab repository `nz_dwelling_structures\inputs`.
#Always make sure to open the `.Rproj` file located in nz_dwelling_structures prior to running, 
#as this will make the `here()` library point to the correct directories when loading inputs and writing outputs. 
construct_largeDwellings <-function(individual_nodelist, backfilled_dwellingSizes,concordance, SEED = 123){
  # libraries
  require(here)
  require(tidyverse)
  require(splitstackshape)
  
  set.seed(SEED)
  # load input
  individual_df <- individual_nodelist
  
  #get number of individuals who are in large dwellings in the node list
  individual_largeDwellings <- individual_df %>% filter(dwellingAssigned == "Y" & smallDwelling == "N") %>%
    group_by(SA22018) %>% 
    summarise(largeDwellingCount = n_distinct(Individual_ID)) %>%
    ungroup()
 
  # individual_largeDwellings %>%
  #   group_by(largeDwellingCount) %>%
  #   summarise(Freq = n()) %>%
  #   ggplot(aes(x = largeDwellingCount, y = Freq)) +
  #   geom_vline(xintercept = 20) +
  #   geom_point()
  
  
  #This function will take a Count_people (M) and break it into N integers (N being the count of dwellings)
  #according to some distribution. log normal seems to fit overall - potentially we can improve here. 
  #function found here: https://stackoverflow.com/questions/24845909/generate-n-random-integers-that-sum-to-m-in-r
  distribution_allocation <- function(N, M, Lwr, Upr, sd = 1, pos.only = TRUE) {
    #get values from distribution (log norm??)
    #vec <- rnorm(N, M/N, sd) #normal
    #vec <- rlnorm(N, M/N, sd=1) # log normal
    vec <- EnvStats::rlnormTrunc(N, log(M/N), sd=1, min =Lwr,max = Upr) #truncated log normal
    
    if (abs(sum(vec)) == Inf) vec <- rep(M/N, N)
    #round the values from the distribution
    vec <- round(vec / sum(vec) * M)
    #rounding will throw some values away from the total we're after - call this deviation
    deviation <- M - sum(vec)
    #reallocate the deviation to sample of values so values add up to correct total
    for (. in seq_len(abs(deviation))) {
      vec[i] <- vec[i <- sample(N, 1)] + sign(deviation)
    }
    if (pos.only) while (any(vec < 0)) {
      negs <- vec <= 0
      pos  <- vec > 0
      vec[negs][i] <- vec[negs][i <- sample(sum(negs), 1)] + 1
      vec[pos][i]  <- vec[pos ][i <- sample(sum(pos ), 1)] - 1
    }
    vec
  }
  
  # load concordance
  #can be found `Dropbox (Complex systems)\covid-19-sharedFiles\data\spatialdata\concordances`
  
  # concordance <- read_csv(here("inputs", "MB18_SA2_majority_concordance_IDI.csv"))
  # 
  # these data files should be in Dropbox/data/IDI_raw_outputs/Dwelling_structure or
  # produced by the backfilling_dwellingTriples_notebook
  
  #The counts provided are very likely inaccurate for large dwellings as counts (even large counts) get suppressed when their are too few unique dwellings. 
  # They can only really give an idea of the existence of a dwelling in that range.
  dwellings_sa2 <- backfilled_dwellingSizes %>%
    rename(dwelling_size = "dwl_size") %>%
    
    #only large dwelling (>20)
    filter(str_detect(dwelling_size, "c020-049|c050-099|c100+")) %>%
    
    #filter out NA counts
    filter(!is.na(count)) %>%
    distinct(SA22018, dwelling_size) %>%
    #get minimum possible dwelling size
    mutate(min_size= parse_number(str_extract(dwelling_size, pattern = "c(...)"))) %>%
    mutate(max_size= parse_number(str_extract(dwelling_size, pattern = "(...)$"))) %>%
    mutate(max_size= ifelse(max_size == 0, Inf, max_size))
  
  
  dwellings_sa2_nodelist_complete <- dwellings_sa2 %>%
    left_join(individual_largeDwellings, by = "SA22018") %>%
    
    #in cases where there are two different dwelling size bands present, we want to have each represented in the final nodelist.
    
    #in cases of multiple rows, we could just change the max size to be the same as the the highest max
    #e.g., instead of 20-49 and 50-99, we would have 20-99. This may undercount the larger dwellings though (this was the first effort).
    
    # group_by(SA22018, largeDwellingCount_new) %>%
    # mutate(min_size = min(min_size),
    #        max_size = ifelse(any(max_size == Inf), Inf, max(max_size))
    #        )%>% select(-dwelling_size) %>% unique()
    
  #to ensure that each band is represented, we will guarantee their presence by assigning the minimum population to rows with smaller ranges
  #we want to prioritise having population to fill the larger dwellings, so we will put the minimum we can in the smaller dwelling ranges
  #and save the rest of the population for the larger dwelling range
  #This means we will have more dwellings sized 20 and 50, as these are the minimum in the ranges. 
  #We may be able to add more noise to this function if needed by randomly assigning the population to include in the smaller band.
  #as it stands, taking the minimum ensures that their is enough population left to fill the larger band.
    group_by(SA22018) %>%
    #identify SA2s that have multiple rows
    mutate(number_rows = n()) %>%
    #identify the row within that SA2 with the smallest range
    mutate(smallest = ifelse(min_size == min(min_size,na.rm = T), T,F)) %>% #does the row have the smallest min size in the group?
    mutate(smallest_size = min(min_size,na.rm = T)) %>% #the largest minimum size in that SA2
    ungroup() %>%
    #this function will make sure the minimum value of the lowest band is included.
    mutate(largeDwellingCount = ifelse(number_rows==1,largeDwellingCount, #if there are multiple rows
                                       ifelse(smallest == T, #if the row contains smallesr dwelling range
                                              smallest_size, #make sure there is enough to include the minimum pop for that dwelling size
                                              largeDwellingCount-smallest_size #subtract this population from the other rows
                                       ))
    ) %>%
    ungroup() %>% 
    
    #repeat the same process since there area few SA2s with the 3 different sizes.
    #these will need to be grouped by SA2 as well as the large dwelling count which is duplicated.
    group_by(SA22018, largeDwellingCount) %>%
    #Identify the cases with multiple rows
    mutate(number_rows = n()) %>%
    #identify the case with the smallest range
    mutate(smallest = ifelse(min_size == min(min_size,na.rm = T), T,F)) %>% #does the row have the lowest min size in the group?
    mutate(smallest_size = min(min_size,na.rm = T)) %>% #the lowest minimum size in that SA2
    ungroup() %>%
    #this function will make sure the minimum value of the lowest band is included.
    mutate(largeDwellingCount = ifelse(number_rows==1,largeDwellingCount, #if there are multiple rows
                                       ifelse(smallest == T, #if the row contains smaller dwelling range
                                              smallest_size, #make sure there is enough to include the minimum population for that dwelling size
                                              largeDwellingCount-smallest_size #subtract this population from the other rows
                                       ))
    ) %>% 
    #remove columns no longer needed
    select(-smallest_size,-smallest,number_rows) %>%
    
    #now we will break the dwelling population into different number of dwellings
    
    #get a guess of how many dwellings. Key questions:
    # - what do we do with dwellings over 100? Depends on the SA2 and what we expect to find. 
    # - university population may be split across multiple dwellings, while a prison is likely 1. 
    #key types (checking top 10) seem to be: university, army camps, prisons 
    
    #if there is fewer than 49 or more than 100 then we will just give it count 1 for now.
    mutate(count_dwelling_guess = ifelse(is.na(min_size)|min_size==100, 1,
                                         floor(largeDwellingCount/(min_size))) #otherwise we will just divide the pop by the min size 
           #(likely overestimates number of dwellings.)
    ) 
  
  #now expand rows to produce a nodelist
  dwellings_sa2_nodelist_expanded <- dwellings_sa2_nodelist_complete %>%
    expandRows(count = "count_dwelling_guess") %>%
    group_by(SA22018,min_size,max_size) %>%
    mutate(dwelling_size = ifelse(is.na(min_size), largeDwellingCount,
                                  distribution_allocation(N = n(),
                                                          M = first(largeDwellingCount),
                                                          Lwr = min_size,
                                                          Upr = max_size))
    ) %>%
    ungroup() %>%
    mutate(in_range = ifelse(dwelling_size >= min_size & dwelling_size <= max_size, T,F)) %>% #for checking if guess at dwelling size fits in the range given
    #add TA
    left_join(concordance %>% select(SA22018,TA2018) %>% unique, by = "SA22018") %>%
    mutate(TA2018 = str_pad(TA2018, width = 2, side = "left", pad=0)) %>%
    group_by(TA2018) %>%
    mutate(Dwelling_ID = paste0("DWL",
                                str_pad(TA2018,width=2,side="left",pad="0"),
                                str_pad(row_number(), width = 7, side="left", pad = "0"))
    ) %>%
    ungroup() %>%
    select(Dwelling_ID, SA22018, TA2018, dwelling_size)


  print(
    dwellings_sa2_nodelist_expanded %>%
    group_by(dwelling_size) %>%
    summarise(Count = n_distinct(Dwelling_ID)) %>%
    ungroup() %>%
    ggplot(aes(x = dwelling_size, y = Count)) +
      geom_vline(xintercept = 20) +
      geom_point(alpha=0.2) +
      scale_x_log10(labels=scales::comma_format(accuracy = 1)) + 
      scale_y_log10(labels=scales::comma_format(accuracy = 1)) + 
      theme_bw()  +
      labs(x="Dwelling Size", y= "Count") +
      ggtitle("Final Large Dwelling Size Distribution")
    )

  # write_csv(dwellings_sa2_nodelist_expanded, here("outputs","largeDwellings_sa2_nodelist.csv"))
  
  # write_csv(dwellings_sa2_nodelist_expanded, here("outputs",paste0("largeDwellings_sa2_nodelist_",format(Sys.Date(),"%Y%m%d"),".csv")))
  
  return(dwellings_sa2_nodelist_expanded)
}

