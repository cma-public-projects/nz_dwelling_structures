# NZ_dwelling_structures

This repository holds data on dwelling structures obtained from the IDI. Using census 2018 data, we provide SA2 level counts of dwellings categorised by the age configurations of inhabitants. 

## Instructions:
- Ensure that the repo is up to date on your machine. The notebook will save all files using the short ID of the git commit that is checked out in file/folder names, if you make changes locally and then run the notebook, it will still use the latest git commit ID. To make sure that file names are consistent with content, make sure to push any changes before running the notebook.
- Open `nz_dwellings.Rproj`. This will open in RStudio. The `.Rproj` file ensures that all specified paths are relative to the main repo directory.
- Within RStudio, open `construct_complete_dwelling_nodelist_with_aged_care.Rmd`. This notebook will call all other code files and should run from start to finish to produce the node list.
- Ensure that all R packages are installed. these include: `here`,`tidyverse`,`splitstackshape`,`git2r`, `EnvStats`, `sf` .
- Set your local path to the dropbox folder `covid-19-sharedFiles/`. Ensure that the final backslash is included. After setting this string, the input files should all have the correct file names. 
- Check the file names (lines 33-75). Ensure that the files have been made available offline on dropbox. If they are stored as "online-only" it will cause an error later on. (ST: After rerunning the code on a new machine, I found that all the bugs that I ran into were to do with the input files not being available offline - even if the error message is not clear). 
- After all of these steps, you should be able to `Run All` chunks (this can be done by navigating to the top right where it says `Run`, and clicking `Run All` on the drop down menu. The code will take roughly 10 minutes to finish, and it should produce a folder with the node list and sense checking files in an `outputs` folder in the repo, as well as a copy that goes to the `dwellings/nodelists/` folder in dropbox. 
- Double check the node list and the associated sense checking files to make sure the file looks reasonable before using. 
- Additional comments and descriptions of functions are described in `construct_complete_dwelling_nodelist_with_aged_care.Rmd`.

Information on specific folders are detailed as follows:

## Folders

### processing

The ` processing` folder contains the code to produce a synthetic set of individual dwellings based on the IDI data. 

It only requires the rmarkdown file `construct_complete_dwelling_nodelist_with_aged_care.Rmd` to be run, with this notebook calling the other R scripts in the correct order to produce the final node list.

The notebook has 3 key steps:

**Retrieval**

**Process**
All input data is loaded into R and processed to create a node list output. The R scripts run are summarised as follows (in order of their implementation):

- `construct_smallDwellings.R`
  - This file takes builds the small dwelling node list, using data from the IDI. The input data (`dwelling_composition_V2_sa2_safe - Checked.csv`) can be found in the Dropbox covid-19-sharedFiles\data\IDI_raw_outputs\Dwelling_structure. The code will  impute suppressed values and generate a dataframe where each row represents a count of 1 dwelling.
  - This covers dwellings sized < 20 (i.e "small" dwellings)
- `get_backfilled_dwellingSizes.R`
  - This file takes the input from Dropbox/data/IDI_raw_outputs/triples/dwellingTriples/dwellingTriples_V2_Checked  and imputes suppressed values.
- `construct_largeDwellings.r`
  - This file takes input on dwelling sizes from Dropbox covid-19-sharedFiles\data\IDI_raw_outputs\Dwelling_structure, information on individuals' dwelling assignment using the individual node list as input (see https://gitlab.com/tpm-public-projects/nz_individuals for code to generate nodelist, or Dropbox covid-19-sharedFiles\data\individuals), and the backfilled dwellingSizes produced by ``get_backfilled_dwellingSizes.R` to impute the dwelling sizes and counts for large dwellings.
- `combine_small_and_large_lists.R`
  - This file takes the outputs produced by `construct_smallDwellings.r` and `construct_largeDwellings` and combines them into one data.frame.
- `agedCare_addSpatial.R`
  - Takes data on aged-care facilities obtained from https://www.health.govt.nz/your-health/certified-providers/aged-care and attaches geospatial information (longitude and latitude, and SA2) based on facilities address. 
  - Requires access to the google maps API with a key.
  - If unable to run, the cleaned output with geospatial information can be found in Dropbox: covid-19-sharedFiles\data\careHomes\processed_data\AgedCareFacilities_clean.csv
- `add_agedCare_to_dwellings.R`
  - This file links aged-care facilities to specific dwellings.
  - It will first attempt to find suitable large dwellings in the same location for each aged-care facility, and if that fails, link to multiple small dwellings defined as housing only 60+ year olds.

**Publish**. All outputs generated within the processing step are copied over to the correct locations in dropbox.


### analysis

The `analysis` folder contains any code used for subsequent analysis of data, such as summary statistics or maps. 

### inputs

The `inputs` folder contains any input files required for `processing` and `analysis`.

### outputs

The `outputs` folder will be the target folder for any results saved by `processing` or  `analysis` files.

## Authors and acknowledgment

Authors (order alphabetical): James Gilmour, Emily Harvey, Dion O'Neale, Steven Turnbull

We would like to acknowledge the help of Statistics NZ, who processed the data. We would also like to acknowledge the help of Adrian Ortiz-Cervantes. This work was funded and supported by Te P\={u}naha Matatini and MBIE.

## License

Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0
International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg
## Disclaimer

The results in this paper are not official statistics. They have been created for research purposes from the Integrated Data Infrastructure (IDI), managed by Statistics New Zealand. The opinions, findings, recommendations, and conclusions expressed in this paper are those of the author(s), not Statistics NZ. Access to the anonymised data used in this study was provided by Statistics NZ under the security and confidentiality provisions of the Statistics Act 1975. Only people authorised by the Statistics Act 1975 are allowed to see data about a particular person, household, business, or organisation, and the results in this paper have been confidentialised to protect these groups from identification and to keep their data safe. Careful consideration has been given to the privacy, security, and confidentiality issues associated with using administrative and survey data in the IDI. Further detail can be found in the Privacy impact assessment for the Integrated Data Infrastructure available from www.stats.govt.nz. 
