#This notebook will build the dwelling node list, using data from the IDI. The input data can be found in the Dropbox
#covid-19-sharedFiles\data\IDI_raw_outputs\Dwelling_structure. Note: This data only includes `dwelling_size < 20
#To run this R script, copy the Dwelling_structure folder to the gitlab repository nz_dwelling_structures\inputs.
#Always make sure to open the .Rproj file located in nz_dwelling_structures prior to running, 
#as this will make the `here()` library point to the correct directories when loading inputs and writing outputs. 

construct_smallDwellings <- function(SEED = 123){
# libraries
library(here)
library(tidyverse)
library(splitstackshape)


# load input

## these data files should be in Dropbox/data/IDI_raw_outputs/Dwelling_structure
dwellings_sa2 <- read_csv(here("inputs", "dwelling_composition_V2_sa2_safe - Checked.csv"))
dwellings_ta <- read_csv(here("inputs", "dwelling_composition_V2_ta_safe - Checked.csv")) %>%
  filter(ur_ta != 999)
dwellings_nz <- read_csv(here("inputs", "dwelling_composition_V2_nz_safe - Checked.csv"))


# load concordance
#can be found `Dropbox (Complex systems)\covid-19-sharedFiles\data\spatialdata\concordances`
# this data needs to be copied from Dropbox/data/spatial and Dropbox/data/IDI_raw_outputs into git-repo/RCode/inputs

concordance_names <-read_csv(here("inputs","SA2_TA18_REGC_concordance.csv"),
                             col_types = cols(
                               SA22018 = col_double(),
                               SA22018_N = col_character(),
                               REGC2018 = col_double(),
                               REGC2018_N = col_character(),
                               TA2018 = col_double(),
                               TA2018_N = col_character(),
                               SA2_area = col_double()
                             )
) %>%
  select(starts_with('SA22018'),starts_with("TA2018")) %>%
  unique()


concordance <- read_csv(here("inputs","MB18_SA2_majority_concordance_IDI.csv"),
                        col_types = cols(
                          MB2018 = col_double(),
                          SA22018 = col_double(),
                          REGC2018 = col_double(),
                          TA2018 = col_double(),
                          DHB2015 = col_double(),
                          DHBCON2015 = col_double()
                        )
) %>%
  select(SA22018,TA2018) %>%
  unique() %>%
  left_join(concordance_names %>% select(SA22018,SA22018_N), by = "SA22018") %>%
  left_join(concordance_names %>% select(TA2018,TA2018_N) %>% unique(), by = "TA2018") %>%
  select(SA22018,SA22018_N,TA2018,TA2018_N)





#Method 1
#Method 1 involves simply expanding the rows of the SA2 level table so a full node list is produced. 
#The majority of dwellings will have full configuration information, but some will be missing info. 
#This may be a strength as it will make the matching process more leniant, and it is easy to do. 
#This method does not involve the NZ or TA level data.


set.seed(SEED) #since we will be randomly allocating counts 1-5 where data is missing, set seed.

#Begin by saving cleaned copies of the NZ, TA and SA2 level data. 

dwellings_nz_clean <- dwellings_nz %>% 
  rowwise() %>%
  #if count_dwellings is missing (999999), then fill in with random number from 1 to 5
  mutate(Count_dwellings = ifelse(Count_dwellings == "999999", sample(1:5),Count_dwellings)) %>%
  #get the number of people by multiplying the dwelling size by the count of those dwellings.
  mutate(Count_people = ifelse(Count_people == "999999", parse_number(x =dwelling_size)*Count_dwellings, Count_people)) %>%
  ungroup() 

write_csv(dwellings_nz_clean,here("outputs","dwelling_composition_V2_nz_safe_cleaned.csv"))

dwellings_ta_clean <- dwellings_ta %>% 
  rowwise() %>%
  #if count_dwellings is missing (999999), then fill in with random number from 1 to 5
  mutate(Count_dwellings = ifelse(Count_dwellings == "999999", sample(1:5),Count_dwellings)) %>%
  #get the number of people by multiplying the dwelling size by the count of those dwellings.
  mutate(Count_people = ifelse(Count_people == "999999", parse_number(x =dwelling_size)*Count_dwellings, Count_people)) %>%
  ungroup() %>%
  mutate(TA2018 = str_pad(ur_ta, width = 2, side = "left", pad=0)) %>% 
  filter(TA2018 != 999) %>% select(TA2018,everything(),-ur_ta)

write_csv(dwellings_ta_clean,here("outputs","dwelling_composition_V2_ta_safe_cleaned.csv"))

#NZ Clean
dwellings_sa2_clean <- dwellings_sa2 %>% 
  rowwise() %>%
  #if count_dwellings is missing (999999), then fill in with random number from 1 to 5
  mutate(Count_dwellings = ifelse(Count_dwellings == "999999", sample(1:5),Count_dwellings)) %>%
  #get the number of people by multiplying the dwelling size by the count of those dwellings.
  mutate(Count_people = ifelse(Count_people == "999999", parse_number(x =dwelling_size)*Count_dwellings, Count_people)) %>%
  ungroup() %>%
  #join concordance an remove TA 999 
  left_join(concordance %>% select(SA22018,TA2018) %>% unique, by = c("ur_sa2" = "SA22018")) %>%
  mutate(TA2018 = str_pad(TA2018, width = 2, side = "left", pad=0)) %>% 
  filter(TA2018 != 999) %>% select(ur_sa2,TA2018, everything())

write_csv(dwellings_sa2_clean,here("outputs","dwelling_composition_V2_sa2_safe_cleaned.csv"))

#expand those we already know (Baseline-Step3)
dwellings_sa2_nodelist <- expandRows(dwellings_sa2_clean, count = "Count_dwellings")


# fill in step 4 counts
#First we need to allocate dwelling sizes to STEP4 aggregation rows using the count of people that should go there.
#We'll allocate these people to different sized dwellings according to a truncated lognormal distribution using the function. 
#distribution_allocation.

set.seed(SEED) #since we will be randomly allocating counts 1-5 where data is missing, set seed.

#This function will take a Count_people (M) and break it into N integers (N being the count of dwellings)
#according to some distribution. log normal seems to fit overall - potentially we can improve here. 
#function found here: https://stackoverflow.com/questions/24845909/generate-n-random-integers-that-sum-to-m-in-r
distribution_allocation <- function(N, M, Lwr, Upr, sd = 1, pos.only = TRUE) {
  #get values from distribution (log norm??)
  #vec <- rnorm(N, M/N, sd) #normal
  #vec <- rlnorm(N, M/N, sd) # log normal
  vec <- EnvStats::rlnormTrunc(N, M/N, sd, min = Lwr,max = Upr) #truncated log normal

  if (abs(sum(vec)) == Inf) vec <- rep(M/N, N)
  #round the values from the distribution
  vec <- round(vec / sum(vec) * M)
  #rounding will throw some values away from the total we're after - call this deviation
deviation <- M - sum(vec)
#reallocate the deviation to sample of values so values add up to correct total
for (. in seq_len(abs(deviation))) {
  vec[i] <- vec[i <- sample(N, 1)] + sign(deviation)
}
#If there are more houses to fill than people to fill them, we will get a 0 count regardless
#to stop code from breaking, we will accomodate this.
if (N>M) {while (any(vec < 0)) {
  negs <- vec <= 0
  pos  <- vec > 0
  vec[negs][i] <- vec[negs][i <- sample(sum(negs), 1)] + 1
  vec[pos][i]  <- vec[pos ][i <- sample(sum(pos ), 1)] - 1
}
  #otherwise we will keep shuffling until all vectors are > 0
}else{
  while (any(vec <= 0)) {
    negs <- vec <= 0
    pos  <- vec > 1
    vec[negs][i] <- vec[negs][i <- sample(sum(negs), 1)] + 1
    vec[pos][i]  <- vec[pos ][i <- sample(sum(pos ), 1)] - 1
  }
}
vec
}

dwellings_sa2_nodelist_complete <- dwellings_sa2_nodelist %>%
  group_by(ur_sa2, Step) %>%
  #apply distribution allocation function
  mutate(dwelling_size = ifelse(Step == "Step4", distribution_allocation(N = n(),
                                                                         M = first(Count_people),
                                                                         Lwr = parse_number(first(dwelling_size)),
                                                                         Upr = 19),
                                as.double(dwelling_size)
  )
  ) %>%
  ungroup() %>%
  #tidy
  rename(SA22018 = "ur_sa2") %>%
  group_by(TA2018) %>%
  #create dwelling ID (these IDs may get recast later on when the network is built)
  mutate(Dwelling_ID = paste0("DWS",
                              str_pad(TA2018,width=2,side="left",pad="0"),
                              str_pad(row_number(), width = 7, side="left", pad = "0"))) %>%
  ungroup() %>%
  select(Dwelling_ID,SA22018, TA2018, everything(),-Count_people)


print(dwellings_sa2_nodelist_complete %>% group_by(dwelling_size) %>%
  summarise(Count_dwellings = n_distinct(Dwelling_ID)) %>%
  ggplot(aes(x = dwelling_size, y = Count_dwellings)) +
    geom_bar(stat="identity") +
    scale_x_continuous(breaks = 1:max(dwellings_sa2_nodelist_complete$dwelling_size)) +
    scale_y_continuous(labels = scales::comma_format(accuracy = 1)) +
    labs(x = "Dwelling Size", y = "Count") +
    theme_bw()+
    ggtitle("Final small dwelling size distribution")
  )


write_csv(dwellings_sa2_nodelist_complete, here("outputs", "smallDwellings_sa2_nodelist.csv"))
write_csv(dwellings_sa2_nodelist_complete, here("outputs", paste0(Sys.Date(),"-smallDwellings_sa2_nodelist.csv")))

return(dwellings_sa2_nodelist_complete)
}
