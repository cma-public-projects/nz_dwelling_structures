
#This code takes constructed small dwellings and large dwellings and combines them into one node list.
combine_dwellings <- function(smallDwellings,largeDwellings){

  require(tidyverse)
  #load small dwellings
  small_dw <- smallDwellings
  #load large dwellings, and put into same structure as small dwellings.
  large_dw <- largeDwellings %>%
    #add columns present in small dwelling nodelist but set as NA
    mutate(ConfigA_0to14=NA,ConfigB_15to29=NA,ConfigC_30to59=NA,
           ConfigD_60plus=NA,Config_adults=NA,Config_workingAge=NA,
           "Step"="LargeDwellings") %>%
    select("Dwelling_ID","SA22018","TA2018","ConfigA_0to14","ConfigB_15to29","ConfigC_30to59",
           "ConfigD_60plus","dwelling_size","Config_adults","Config_workingAge","Step")
  
  if(nrow(smallDwellings %>% filter(dwelling_size <1))>0){warning("Dwellings of size <1 exist in small dwelling nodes")}
  if(nrow(largeDwellings %>% filter(dwelling_size <1))>0){warning("Dwellings of size <1 exist in large dwelling nodes")}
  
  all_dw <- bind_rows(small_dw,large_dw) %>% arrange(SA22018) %>%
    
    filter(dwelling_size>0) %>%
    group_by(TA2018) %>%
    #create dwelling ID (these IDs may get recast later on when the network is built)
    mutate(Dwelling_ID = paste0("DW",
                                str_pad(TA2018,width=2,side="left",pad="0"),
                                str_pad(row_number(), width = 7, side="left", pad = "0"))) %>%
    ungroup() 
  
  #save nodelist
  # write_csv(all_dw,here("outputs","dwelling_sa2_nodelist_complete.csv"))
  # write_csv(all_dw,here("outputs",paste0(Sys.Date(),"-dwelling_sa2_nodelist_complete.csv")))
  
  print(
    all_dw %>%
      group_by(dwelling_size) %>%
      summarise(Count = n_distinct(Dwelling_ID)) %>%
      ungroup() %>%
      ggplot(aes(x = dwelling_size, y = Count)) +
      geom_vline(xintercept = 20) +
      geom_point(alpha=0.2) +
      scale_x_log10(labels=scales::comma_format(accuracy = 1)) + 
      scale_y_log10(labels=scales::comma_format(accuracy = 1)) + 
      theme_bw()  +
      labs(x="Dwelling Size", y= "Count") +
      ggtitle("Final Dwelling Size Distribution (Small and Large Combined)")
  )
  
  return(all_dw)
}
