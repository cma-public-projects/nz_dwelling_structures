---
title: "getHouseholdStructure"
output: html_notebook
---

# Background
This notebook will produce a nodelist of dwellings, based on the census individual table.
It will use the same SQL query as used in the dwelling_triples data (our base population)

# Libraries
```{R - Libraries, message = FALSE}
#Import necessary libraries
library(RODBC)
library(dplyr)
library(tidyr)
library(reshape2)
library(stringr)
library(magrittr)
library(readr)
library(ggplot2)
library(forcats)
library(here)  # needs installing locally
here()
```

```{R Random Rounding Function}
#Random rounding
rrn <- function(x, n=3, seed)
{
  if (!missing(seed)) set.seed(seed)
  
  rr <- function(x, n){
    if (is.na(x)) return(0)
    if ((x%%n)==0) return(x)
    res <- abs(x)
    lo <- (res%/%n) * n
    if ((runif(1) * n) <= res%%n) res <- lo + n
    else res <- lo
    return(ifelse(x<0, (-1)*res, res))
  }
  
  isint <- function(x){
    x <- x[!is.na(x)]
    sum(as.integer(x)==x)==length(x)
  }
  
  if (class(x) %in% c("numeric", "integer")){
    if(isint(x)) return(sapply(x, rr, n))
    else return(x)
  }
  
  for (i in 1:ncol(x))
  {
    if (class(x[,i]) %in% c("numeric", "integer") &
        isint(x[,i])) x[,i] <- sapply(x[,i], rr, n)
  }
  x
}

```


```{R SQL Import}
sql_query <- read_file("triplesPerDwelling.sql")
  
  #open connection to sql
con=odbcDriverConnect("Driver=ODBC Driver 17 for SQL Server; Trusted_Connection=YES; Server=PRTPRDSQL36.stats.govt.nz,1433;Database=IDI_Clean_20200120")

  
#Run query and save as df
df <- sqlQuery(con, sql_query) 
  #close connection
odbcCloseAll()

#population count test
print(paste0("Population from sql: ", sum(df$triple_count)))
```

```{R Clean df}
# checking all 'sex' entries are 1 or 2
if(df %>% filter(!(sex %in% c(1,2))) %>% nrow()!= 0){message('There are some entries with no values for sex')}

#Recode sex from 1 and 2 to M and F (checked using 2018 Census variables metadata file on IDI wiki)
df_clean <- df %>% mutate(sex = recode(sex,
                                             `1` = "M",
                                             `2` = "F")
                                )


# tidying ethnicity

df_clean <- df_clean %>%
  mutate(ethnicity = case_when(
    is_maori==0 & is_pacific == 0  ~  'Other',
    is_maori==0 & is_pacific == 30  ~  'Pacific',
    is_maori==20 & is_pacific == 0  ~  'Maori',
    is_maori==20 & is_pacific == 30  ~  'MaoriPacific',
    TRUE  ~ "NA"
    )
  ) %>%
  mutate(ethnicity = factor(ethnicity,
                            levels = c(
                            'MaoriPacific',
                            'Maori',
                            'Pacific',
                            'Other'))
         ) %>% select(-c(is_maori,is_pacific))

# check
if(df_clean %>% filter(is.na(ethnicity)) %>% nrow()!= 0) {message( 'There are some entries with unrecognised entries for ethnicity')}

if(df_clean %>% filter(is.na(age_band)) %>% nrow()!= 0) {message( 'There are some entries with unrecognised entries for age')}

```

# get a 'real' dwelling nodelist
The following chunk produces df_dwelling_nodelist, a dataframe which is a real dwelling nodelist based on census data (N ~ 1.6 million).
This is **Table B** in the `dataProposal_HouseholdStructure.md`.
Our goal is to best reconstruct this. 
As of 22-10-2020, the code removes sex and ethnicity information as dwelling node attributes.
 
```{R}
#Now structure dataframe in nodelist format.
#As of 22-10-2020, the current plan is just to have households configured by age_bands, with the plan to introduce sex, ethnicity homophily through the matching process.
df_dwelling_nodelist <- df_clean %>%
  

  #clean age_band strings
  mutate(age_band = case_when(age_band == "0-14" ~ "A_0to14",
                              age_band == "15-29" ~ "B_15to29",
                              age_band == "30-59" ~ "C_30to59",
                              age_band == "60+" ~ "D_60plus",
                              age_band == TRUE ~ "NA")) %>%
  #sum up counts to remove sex,ethnicity groups
  group_by(ur_dwl_id,age_band,ur_sa2,ur_ta) %>%
  summarise(Count_individuals_ageband = sum(triple_count)) %>%
  ungroup() %>%
  #now pivot wider on age_band
  pivot_wider(names_from = age_band,
              names_prefix = "Config",
              values_from = Count_individuals_ageband, 
              values_fill = 0) %>%

  mutate(dwelling_size = ConfigA_0to14+ConfigB_15to29+ConfigC_30to59+ConfigD_60plus) %>%
  #arrange columns
  select(ur_ta,ur_sa2,ur_dwl_id,ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size)
  
  
```

# filter large dwellings
We can remove dwellings with a large number of inhabitants (i.e., prisons, boarding houses, etc). If we want to treat those differently, we filter them out here. 
```{R}
df_dwelling_nodelist <- df_dwelling_nodelist %>%
  filter(dwelling_size < 20)
```


# produce aggregated tables of the nodelist

The dataframes produced are in an aggregated format that can be extracted from the IDI. Once out of the IDI, we can expand rows to produce a synthetic nodelist.

For each geospatial resolution, the following logic is applied to preserve as much household structure as possible before confidentialisation and extraction.
For any geospatial resolution, if a household configuration would get suppressed, the following STEPS are taken (note, the following is used to descibe household configuration: A|B|C|D, where A is 0-14 count, B is 15-29 count, C is 30-59 count, and D is 60+ count):


- STEP 1: we remove age_bands, and instead use a count of children, count of working age adults, and 60+. Children are just the same as the count in the 0-14 age_band, while adults are considered as the sum of the other three bands (15-29, 30-59). This means that the counts of the following configurations would be combined: `2|1|1|1` and `2|0|2|1` as both get presented as `2 (children) |2 (adults)| 1 (older)`

- STEP 2: we remove age_bands, and instead use a count of children and a count of adults. Children are just the same as the count in the 0-14 age_band, while adults are considered as the sum of the other three bands (15-29, 30-59, 60+). This means that the counts of the following configurations would be combined: `2|1|1|1` and `2|0|2|1` as both get presented as `2 (children) |3 (adults)`

- STEP 3: we remove info on adult and children counts. This leaves us with a total dwelling size, but no other information.  

- STEP 4: we make dwelling size equal to the minimum dwelling size for those rows left suppressed. This will include number of dwellings and number of people.  

# Whole NZ level
```{R}
options(scipen = 999)
# first reformat nodelist to aggregated rows, and apply logic to minimise suppression
df_dwelling_composition_nz <- df_dwelling_nodelist %>%
  mutate(Step = "Baseline") %>%
  group_by(ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size, Step) %>%
  #count of distinct dwellings  comprised of these agebands
  summarise(Count_dwellings = n_distinct(ur_dwl_id)) %>%
  ungroup() %>%
  #get count of people
  mutate(Count_people = dwelling_size*Count_dwellings) %>%
  #get count of adults
  mutate(Config_adults = ConfigB_15to29 + ConfigC_30to59 + ConfigD_60plus) %>%
   #get count of adults
  mutate(Config_workingAge = ConfigB_15to29 + ConfigC_30to59) %>%
  # get indicator of levels of suppression, next steps apply logic to minimise
  mutate(Suppressed = ifelse(Count_dwellings <6|Count_people<6, "Y", "N")) %>%
  # print level of dwellings covered
  (function(x){print(paste0("Baseline: ", round(sum(x$Count_dwellings[x$Suppressed == "N"])/sum(x$Count_dwellings)*100,2),"%")); return(x)}) %>%


   ## STEP 1: if configuration still gets suppressed, remove have child/ working age/ older age counts)
  mutate(Step = ifelse(Suppressed == "Y", "Step1", Step)) %>%  
  mutate_at(.vars = vars(ConfigB_15to29,ConfigC_30to59),
            .funs = funs(ifelse(Suppressed =="Y", NA,.))
            ) %>%
      # flatten
  group_by(ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size, Config_adults, Config_workingAge, Step) %>%
    summarise(Count_dwellings = sum(Count_dwellings),
              Count_people = sum(Count_people)) %>%
  ungroup() %>%
      #Recheck suppression
  mutate(Suppressed = ifelse(Count_dwellings <6|Count_people<6, "Y", "N")) %>%
   # print level of dwellings covered
  (function(x){print(paste0("Step 1: ", round(sum(x$Count_dwellings[x$Suppressed == "N"])/sum(x$Count_dwellings)*100,2),"%")); return(x)}) %>%
  
  
 
  
  ## STEP 2: if configuration still gets suppressed, remove age counts (still have child/adult count)
  mutate(Step = ifelse(Suppressed == "Y", "Step2", Step)) %>%  
  mutate_at(.vars = vars(ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,Config_workingAge),
              #if configuration will get suppressed, reduce age_band info to number of children and adults
            .funs = funs(ifelse(Suppressed =="Y", NA,.))
            ) %>%
      # flatten
  group_by(ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size, Config_adults, Config_workingAge,Step) %>%
    summarise(Count_dwellings = sum(Count_dwellings),
              Count_people = sum(Count_people)) %>%
  ungroup() %>%
      #Recheck suppression
  mutate(Suppressed = ifelse(Count_dwellings <6|Count_people<6, "Y", "N")) %>%
   # print level of dwellings covered
  (function(x){print(paste0("Step 2: ", round(sum(x$Count_dwellings[x$Suppressed == "N"])/sum(x$Count_dwellings)*100,2),"%")); return(x)}) %>%
  
  
  
  ## STEP 3, if configuration still suppressed, remove adult/child counts as well
  mutate(Step = ifelse(Suppressed == "Y", "Step3", Step)) %>%  
  mutate(Config_adults = ifelse(Suppressed == "Y",NA, Config_adults),
         ConfigA_0to14 = ifelse(Suppressed == "Y",NA, ConfigA_0to14)
         ) %>% 
      # flatten
  group_by(ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size, Config_adults,Config_workingAge,Step) %>%
    summarise(Count_dwellings = sum(Count_dwellings),
              Count_people = sum(Count_people)) %>%
  ungroup() %>%
      #Recheck suppression
  mutate(Suppressed = ifelse(Count_dwellings <6|Count_people<6, "Y", "N")) %>%
     # print level of dwellings covered
  (function(x){print(paste0("Step 3: ", round(sum(x$Count_dwellings[x$Suppressed == "N"])/sum(x$Count_dwellings)*100,2),"%")); return(x)}) %>%
  
  
   ## STEP 4, if configuration still suppressed, combine dwelling sizes by using minimum occupancy
  mutate(Step = ifelse(Suppressed == "Y", "Step4", Step)) %>%  
  group_by(ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus, Config_adults,Config_workingAge,Step) %>%
  mutate(dwelling_size = ifelse(Suppressed == "Y", paste0(min(dwelling_size, na.rm = T), "_or_more"), as.character(dwelling_size))) %>% #select minimum and use as new dwelling size
  ungroup() %>% 
      # flatten
  group_by(ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size, Config_adults,Config_workingAge,Step) %>%
    summarise(Count_dwellings = sum(Count_dwellings),
              Count_people = sum(Count_people)) %>%
  ungroup() %>%
      #Recheck suppression
  mutate(Suppressed = ifelse(Count_dwellings <6|Count_people<6, "Y", "N")) %>%
     # print level of dwellings covered
  (function(x){print(paste0("Step 4: ", round(sum(x$Count_dwellings[x$Suppressed == "N"])/sum(x$Count_dwellings)*100,2),"%")); return(x)}) %>%
  
  
  # Work out share of each household configuration in terms of number of dwellings.  
  mutate(NZ_prop = Count_dwellings/sum(Count_dwellings)) %>%
  select(-Suppressed)
df_dwelling_composition_nz

write_csv(df_dwelling_composition_nz,  here("outputs", "dwelling_composition_V2_nz_raw.csv"))
```
# Whole NZ - Confidentiality and Save
```{R}
#now apply confidentility rules and check suppression. 

#Confidentiality rules
#suppression of counts if unique entities are below 3, or if number of people is lower than 6. Do for both Count_dwellings and Count_people 
df_dwelling_composition_nz_safe <- df_dwelling_composition_nz %>%
  mutate(Count_dwellings = ifelse(Count_dwellings <6|Count_people<6, -999999, Count_dwellings),
         Count_people = ifelse(Count_dwellings <6|Count_people<6, -999999, Count_people),
         NZ_prop = ifelse(Count_dwellings <6|Count_people<6, NA, NZ_prop)

         )  


#random rounding for dwelling counts 
df_dwelling_composition_nz_safe$Count_dwellings <- rrn(df_dwelling_composition_nz_safe$Count_dwellings,n = 3)

#random rounding for people counts 
df_dwelling_composition_nz_safe$Count_people <- rrn(df_dwelling_composition_nz_safe$Count_people,n = 3)
#but if the we can work out the people count from the rounded count_dwelling X dwelling size, use that
df_dwelling_composition_nz_safe <- df_dwelling_composition_nz_safe %>%
  mutate(dwelling_size_digit = as.integer(str_remove_all(dwelling_size, "_or_more"))) %>% #grab digit from string
  mutate(Count_people = ifelse(!str_detect(dwelling_size, ".*_or_more"), dwelling_size_digit*Count_dwellings, Count_people)) %>% #if we have the actual dwelling size (not "or more"), make Count people bet dwelling size X count_dwelling
  select(-dwelling_size_digit)
  #totalling over NZ, how many dwellings we kept 
cat(paste0("Dwelling coverage around ", round(sum(df_dwelling_composition_nz_safe$NZ_prop, na.rm = T),2)*100,"%. "))


write_csv(df_dwelling_composition_nz_safe %>% select(-NZ_prop), here("outputs", "dwelling_composition_V2_nz_safe.csv"))
```

# TA level
```{R}
options(scipen = 999)
# first reformat nodelist to aggregated rows, and apply logic to minimise suppression
df_dwelling_composition_ta <- df_dwelling_nodelist %>%
  mutate(Step = "Baseline") %>%
  group_by(ur_ta,ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size, Step) %>%
  #count of distinct dwellings  comprised of these agebands
  summarise(Count_dwellings = n_distinct(ur_dwl_id)) %>%
  ungroup() %>%
  #get count of people
  mutate(Count_people = dwelling_size*Count_dwellings) %>%
  #get count of adults
  mutate(Config_adults = ConfigB_15to29 + ConfigC_30to59 + ConfigD_60plus) %>%
   #get count of adults
  mutate(Config_workingAge = ConfigB_15to29 + ConfigC_30to59) %>%
  # get indicator of levels of suppression, next steps apply logic to minimise
  mutate(Suppressed = ifelse(Count_dwellings <6|Count_people<6, "Y", "N")) %>%
  # print level of dwellings covered
  (function(x){print(paste0("Baseline: ", round(sum(x$Count_dwellings[x$Suppressed == "N"])/sum(x$Count_dwellings)*100,2),"%")); return(x)}) %>%


  #when configuration is due to be suppressed, we will reduce info so more dwellings get grouped together
 

   ## STEP 1: if configuration still gets suppressed, remove have child/ working age/ older age counts)
  mutate(Step = ifelse(Suppressed == "Y", "Step1", Step)) %>%  
  mutate_at(.vars = vars(ConfigB_15to29,ConfigC_30to59),
            .funs = funs(ifelse(Suppressed =="Y", NA,.))
            ) %>%

      # flatten
  group_by(ur_ta,ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size, Config_adults, Config_workingAge, Step) %>%
    summarise(Count_dwellings = sum(Count_dwellings),
              Count_people = sum(Count_people)) %>%
  ungroup() %>%
      #Recheck suppression
  mutate(Suppressed = ifelse(Count_dwellings <6|Count_people<6, "Y", "N")) %>%
   # print level of dwellings covered
  (function(x){print(paste0("Step 1: ", round(sum(x$Count_dwellings[x$Suppressed == "N"])/sum(x$Count_dwellings)*100,2),"%")); return(x)}) %>%
  
  
 
  
  ## STEP 2: if configuration still gets suppressed, remove age counts (still have child/adult count)
  mutate(Step = ifelse(Suppressed == "Y", "Step2", Step)) %>%  
  mutate_at(.vars = vars(ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,Config_workingAge),
              #if configuration will get suppressed, reduce age_band info to number of children and adults
            .funs = funs(ifelse(Suppressed =="Y", NA,.))
            ) %>%

      # flatten
  group_by(ur_ta,ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size, Config_adults, Config_workingAge,Step) %>%
    summarise(Count_dwellings = sum(Count_dwellings),
              Count_people = sum(Count_people)) %>%
  ungroup() %>%
      #Recheck suppression
  mutate(Suppressed = ifelse(Count_dwellings <6|Count_people<6, "Y", "N")) %>%
   # print level of dwellings covered
  (function(x){print(paste0("Step 2: ", round(sum(x$Count_dwellings[x$Suppressed == "N"])/sum(x$Count_dwellings)*100,2),"%")); return(x)}) %>%
  
  
  
  ## STEP 3, if configuration still suppressed, remove adult/child counts as well
  mutate(Step = ifelse(Suppressed == "Y", "Step3", Step)) %>%  
  mutate(Config_adults = ifelse(Suppressed == "Y",NA, Config_adults),
         ConfigA_0to14 = ifelse(Suppressed == "Y",NA, ConfigA_0to14)
         ) %>% 
      # flatten
  group_by(ur_ta,ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size, Config_adults,Config_workingAge,Step) %>%
    summarise(Count_dwellings = sum(Count_dwellings),
              Count_people = sum(Count_people)) %>%
  ungroup() %>%
      #Recheck suppression
  mutate(Suppressed = ifelse(Count_dwellings <6|Count_people<6, "Y", "N")) %>%
     # print level of dwellings covered
  (function(x){print(paste0("Step 3: ", round(sum(x$Count_dwellings[x$Suppressed == "N"])/sum(x$Count_dwellings)*100,2),"%")); return(x)}) %>%
  
  
   ## STEP 4, if configuration still suppressed, combine dwelling sizes by using minimum occupancy
  mutate(Step = ifelse(Suppressed == "Y", "Step4", Step)) %>%  
  group_by(ur_ta,ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus, Config_adults,Config_workingAge,Step) %>%
 
  mutate(dwelling_size = ifelse(Suppressed == "Y", paste0(min(dwelling_size, na.rm = T), "_or_more"), as.character(dwelling_size))) %>% #select minimum and use as new dwelling size
  ungroup() %>% 
  
      # flatten
  group_by(ur_ta,ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size, Config_adults,Config_workingAge, Step) %>%
    summarise(Count_dwellings = sum(Count_dwellings),
              Count_people = sum(Count_people)) %>%
  ungroup() %>%
      #Recheck suppression
  mutate(Suppressed = ifelse(Count_dwellings <6|Count_people<6, "Y", "N")) %>%
     # print level of dwellings covered
  (function(x){print(paste0("Step 4: ", round(sum(x$Count_dwellings[x$Suppressed == "N"])/sum(x$Count_dwellings)*100,2),"%")); return(x)}) %>%
  
  
  # Work out share of each household configuration in terms of number of dwellings.  
  group_by(ur_ta) %>%
  mutate(TA_prop = Count_dwellings/sum(Count_dwellings)) %>%
  ungroup() %>%
  select(-Suppressed)

df_dwelling_composition_ta

write_csv(df_dwelling_composition_ta, here("outputs", "dwelling_composition_V2_ta_raw.csv"))
  
```
# TA - Confidentiality and Save
```{R}
#now apply confidentility rules and check suppression. 

#Confidentiality rules
#suppression of counts if unique entities are below 3, or if number of people is lower than 6. Do for both Count_dwellings and Count_people 
df_dwelling_composition_ta_safe <- df_dwelling_composition_ta %>%
  mutate(Count_dwellings = ifelse(Count_dwellings <6|Count_people<6, -999999, Count_dwellings),
         Count_people = ifelse(Count_dwellings <6|Count_people<6, -999999, Count_people),
         TA_prop = ifelse(Count_dwellings <6|Count_people<6, NA, TA_prop)

         ) 
#random rounding for dwelling counts 
df_dwelling_composition_ta_safe$Count_dwellings <- rrn(df_dwelling_composition_ta_safe$Count_dwellings,n = 3)

#random rounding for people counts 
df_dwelling_composition_ta_safe$Count_people <- rrn(df_dwelling_composition_ta_safe$Count_people,n = 3)
#but if the we can work out the people count from the rounded count_dwelling X dwelling size, use that
df_dwelling_composition_ta_safe <- df_dwelling_composition_ta_safe %>%
  mutate(dwelling_size_digit = as.integer(str_remove_all(dwelling_size, "_or_more"))) %>% #grab digit from string
  mutate(Count_people = ifelse(!str_detect(dwelling_size, ".*_or_more"), dwelling_size_digit*Count_dwellings, Count_people)) %>% #if we have the actual dwelling size (not "or more"), make Count people bet dwelling size X count_dwelling
  select(-dwelling_size_digit)
  
write_csv(df_dwelling_composition_ta_safe %>% select(-TA_prop), here("outputs", "dwelling_composition_V2_ta_safe.csv"))
```

```{R}
#totalling over TAs, how well did we fill (max would be 76/76 - i.e., each TA had a total prop of 1)
cat(paste0("Dwelling coverage for around ",round(sum(df_dwelling_composition_ta_safe$TA_prop, na.rm=T)/n_distinct(df_dwelling_composition_ta$ur_ta), 2)*100, " percent.", "(",round(sum(df_dwelling_composition_ta_safe$TA_prop, na.rm=T),2), " out of max number ",n_distinct(df_dwelling_composition_ta$ur_ta),")"))

#plot to show more detail of suppression
df_dwelling_composition_ta_safe %>%
  group_by(ur_ta) %>%
  summarise(missing_prop = 1 - sum(TA_prop,na.rm = T)) %>%
  ungroup() %>%
  ggplot(aes(x = as.character(ur_ta), y = missing_prop)) +
  geom_point(stat="identity") +
  theme_classic()
           
```


# SA2 level
```{R}
options(scipen = 999)
# first reformat nodelist to aggregated rows, and apply logic to minimise suppression
df_dwelling_composition_sa2 <- df_dwelling_nodelist %>%
  mutate(Step = "Baseline") %>%
  group_by(ur_sa2,ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size, Step) %>%
  #count of distinct dwellings  comprised of these agebands
  summarise(Count_dwellings = n_distinct(ur_dwl_id)) %>%
  ungroup() %>%
  #get count of people
  mutate(Count_people = dwelling_size*Count_dwellings) %>%
  #get count of adults
  mutate(Config_adults = ConfigB_15to29 + ConfigC_30to59 + ConfigD_60plus) %>%
   #get count of adults
  mutate(Config_workingAge = ConfigB_15to29 + ConfigC_30to59) %>%
  # get indicator of levels of suppression, next steps apply logic to minimise
  mutate(Suppressed = ifelse(Count_dwellings <6|Count_people<6, "Y", "N")) %>%
  # print level of dwellings covered
  (function(x){print(paste0("Baseline: ", round(sum(x$Count_dwellings[x$Suppressed == "N"])/sum(x$Count_dwellings)*100,2),"%")); return(x)}) %>%


  #when configuration is due to be suppressed, we will reduce info so more dwellings get grouped together
 
   ## STEP 1: if configuration still gets suppressed, remove have child/ working age/ older age counts)
  mutate(Step = ifelse(Suppressed == "Y", "Step1", Step)) %>%  
  mutate_at(.vars = vars(ConfigB_15to29,ConfigC_30to59),
            .funs = funs(ifelse(Suppressed =="Y", NA,.))
            ) %>%

      # flatten
  group_by(ur_sa2,ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size, Config_adults, Config_workingAge, Step) %>%
    summarise(Count_dwellings = sum(Count_dwellings),
              Count_people = sum(Count_people)) %>%
  ungroup() %>%
      #Recheck suppression
  mutate(Suppressed = ifelse(Count_dwellings <6|Count_people<6, "Y", "N")) %>%
   # print level of dwellings covered
  (function(x){print(paste0("Step 1: ", round(sum(x$Count_dwellings[x$Suppressed == "N"])/sum(x$Count_dwellings)*100,2),"%")); return(x)}) %>%
  

 
  
  ## STEP 2: if configuration still gets suppressed, remove age counts (still have child/adult count)
  mutate(Step = ifelse(Suppressed == "Y", "Step2", Step)) %>%  
  mutate_at(.vars = vars(ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,Config_workingAge),
              #if configuration will get suppressed, reduce age_band info to number of children and adults
            .funs = funs(ifelse(Suppressed =="Y", NA,.))
            ) %>%
      # flatten
  group_by(ur_sa2,ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size, Config_adults, Config_workingAge,Step) %>%
    summarise(Count_dwellings = sum(Count_dwellings),
              Count_people = sum(Count_people)) %>%
  ungroup() %>%
      #Recheck suppression
  mutate(Suppressed = ifelse(Count_dwellings <6|Count_people<6, "Y", "N")) %>%
   # print level of dwellings covered
  (function(x){print(paste0("Step 2: ", round(sum(x$Count_dwellings[x$Suppressed == "N"])/sum(x$Count_dwellings)*100,2),"%")); return(x)}) %>%
  
  
  
  ## STEP 3, if configuration still suppressed, remove adult/child counts as well
  mutate(Step = ifelse(Suppressed == "Y", "Step3", Step)) %>%  
  mutate(Config_adults = ifelse(Suppressed == "Y",NA, Config_adults),
         ConfigA_0to14 = ifelse(Suppressed == "Y",NA, ConfigA_0to14)
         ) %>% 
      # flatten
  group_by(ur_sa2,ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size, Config_adults,Config_workingAge,Step) %>%
    summarise(Count_dwellings = sum(Count_dwellings),
              Count_people = sum(Count_people)) %>%
  ungroup() %>%
      #Recheck suppression
  mutate(Suppressed = ifelse(Count_dwellings <6|Count_people<6, "Y", "N")) %>%
     # print level of dwellings covered
  (function(x){print(paste0("Step 3: ", round(sum(x$Count_dwellings[x$Suppressed == "N"])/sum(x$Count_dwellings)*100,2),"%")); return(x)}) %>%
  
  
   ## STEP 4, if configuration still suppressed, combine dwelling sizes by using minimum occupancy
  mutate(Step = ifelse(Suppressed == "Y", "Step4", Step)) %>%  
  group_by(ur_sa2,ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus, Config_adults,Config_workingAge,Step) %>%
  mutate(dwelling_size = ifelse(Suppressed == "Y", paste0(min(dwelling_size, na.rm = T), "_or_more"), as.character(dwelling_size))) %>% #select minimum and use as new dwelling size
  ungroup() %>% 
      # flatten
  group_by(ur_sa2,ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,dwelling_size, Config_adults,Config_workingAge,Step) %>%
    summarise(Count_dwellings = sum(Count_dwellings),
              Count_people = sum(Count_people)) %>%
  ungroup() %>%
      #Recheck suppression
  mutate(Suppressed = ifelse(Count_dwellings <6|Count_people<6, "Y", "N")) %>%
     # print level of dwellings covered
  (function(x){print(paste0("Step 4: ", round(sum(x$Count_dwellings[x$Suppressed == "N"])/sum(x$Count_dwellings)*100,2),"%")); return(x)}) %>%
  
  
  # Work out share of each household configuration in terms of number of dwellings.  
  group_by(ur_sa2) %>%
  mutate(SA2_prop = Count_dwellings/sum(Count_dwellings)) %>%
  ungroup()%>%
  select(-Suppressed)

df_dwelling_composition_sa2

write_csv(df_dwelling_composition_sa2, here("outputs", "dwelling_composition_V2_sa2_raw.csv"))
```
# SA2 - Confidentiality and Save
```{R}
#now apply confidentility rules and check suppression. 

#Confidentiality rules
#suppression of counts if unique entities are below 3, or if number of people is lower than 6. Do for both Count_dwellings and Count_people 
df_dwelling_composition_sa2_safe <- df_dwelling_composition_sa2 %>%
  mutate(Count_dwellings = ifelse(Count_dwellings <6|Count_people<6, -999999, Count_dwellings),
         Count_people = ifelse(Count_dwellings <6|Count_people<6, -999999, Count_people),
         SA2_prop = ifelse(Count_dwellings <6|Count_people<6, NA, SA2_prop)

         ) 
#random rounding for people counts 
df_dwelling_composition_sa2_safe$Count_dwellings <- rrn(df_dwelling_composition_sa2_safe$Count_dwellings,n = 3)

#random rounding for people counts 
df_dwelling_composition_sa2_safe$Count_people <- rrn(df_dwelling_composition_sa2_safe$Count_people,n = 3)
#but if the we can work out the people count from the rounded count_dwelling X dwelling size, use that
df_dwelling_composition_sa2_safe <- df_dwelling_composition_sa2_safe %>%
  mutate(dwelling_size_digit = as.integer(str_remove_all(dwelling_size, "_or_more"))) %>% #grab digit from string
  mutate(Count_people = ifelse(!str_detect(dwelling_size, ".*_or_more"), dwelling_size_digit*Count_dwellings, Count_people)) %>% #if we have the actual dwelling size (not "or more"), make Count people bet dwelling size X count_dwelling
  select(-dwelling_size_digit)

write_csv(df_dwelling_composition_sa2_safe %>% select(-SA2_prop), here("outputs", "dwelling_composition_V2_sa2_safe.csv"))
```

```{R}
#totalling over SA2s, how well did we fill (max would be 2125/2125 - i.e., each SA2 had a total prop of 1)
cat(paste0("Dwelling coverage for around  ", round(sum(df_dwelling_composition_sa2_safe$SA2_prop, na.rm=T)/n_distinct(df_dwelling_composition_sa2$ur_sa2), 2)*100, " percent.", "(", round(sum(df_dwelling_composition_sa2_safe$SA2_prop, na.rm=T),2), " out of max number ",n_distinct(df_dwelling_composition_sa2$ur_sa2),")"))

#plot to show more detail of suppression
df_dwelling_composition_sa2_safe %>%
  group_by(ur_sa2) %>%
  summarise(missing_prop = 1 - sum(SA2_prop,na.rm = T)) %>%
  ungroup() %>%
  ggplot(aes(x = as.character(ur_sa2), y = missing_prop)) +
  geom_point(stat="identity") +
  theme_classic()
           
```
# how many dwellings and people do we get?
```{R}
Number_dwellings_raw<- sum(df_dwelling_composition_sa2$Count_dwellings[df_dwelling_composition_sa2$Count_dwellings != -999999])
Number_people_raw <- sum(df_dwelling_composition_sa2$Count_people[df_dwelling_composition_sa2$Count_people != -999999])

Number_dwellings_safe<- sum(df_dwelling_composition_sa2_safe$Count_dwellings[df_dwelling_composition_sa2_safe$Count_dwellings != -999999])
Number_people_safe <- sum(df_dwelling_composition_sa2_safe$Count_people[df_dwelling_composition_sa2_safe$Count_people != -999999])

print(paste0("Number of dwellings: ", Number_dwellings_safe, " (we lose ", Number_dwellings_raw - Number_dwellings_safe," dwellings through suppression)."))

print(paste0("Number of people: ", Number_people_safe, " (we lose ", Number_people_raw - Number_people_safe," people through suppression)."))
  
df_dwelling_composition_sa2_safe %>% filter(Count_people == -999999)
```
#can we map it?
show a map with SA2s filled according to the proportion of dwellings that will be suppressed.
```{R}
#get shapefile - we have aailable by meshblock
shape_mb_df <-sf::read_sf(here("Shape_files", "meshblock-2018-clipped-generalised.shp"))
#join SA2 concordance
shape_sa2_df <- shape_mb_df %>%
  left_join(read_csv(here("Shape_files", "geographic-areas-file-2018.csv")) %>%
              select("MB2018_code","SA22018_code") %>% unique(),
            by = c("MB2018_V1_" = "MB2018_code")
  )

map_missing_df <- df_dwelling_composition_sa2_safe %>%
  group_by(ur_sa2) %>%
  summarise(missing_prop = 1 - sum(SA2_prop, na.rm = T)) %>%
  ungroup() %>%
  #join shape file
  left_join(shape_sa2_df, by = c("ur_sa2" = "SA22018_code")) 
  
#plot on map by meshblock, meshblocks in the same SA2 will have the same value.
map_missing_df %>%
  ggplot() + 
  geom_sf(aes(geometry = geometry, fill = missing_prop), color = alpha("gray",0.0)) +
  scale_fill_gradientn(colors= c(viridis::viridis_pal()(20)),
                       na.value = "gray97") + 
  theme_void()
 
```
#check dwelling types in these suppressed areas
```{R}
suppressed_sa2s <- df_dwelling_composition_sa2_safe %>%
  group_by(ur_sa2) %>%
  mutate(missing_prop = 1 - sum(SA2_prop, na.rm = T)) %>%
  ungroup() %>% 
  filter(Count_dwellings == -999999)

#check these SA2s in unsupressed dataframe for count_dwellings, count_people  
fullysuppressed_SA2s <- df_dwelling_composition_sa2 %>% semi_join(suppressed_sa2s) %>%
  group_by(ur_sa2) %>% mutate(SA2_pop = sum(Count_people),
                              SA2_dwellings = sum(Count_dwellings)) %>%
  ungroup()
  
```