---
title: "DwellingNodelist_V2"
author: "Steven Turnbull"
date: "08/03/2021"
---

This notebook will build the dwelling node list, using data from the IDI. The input data can be found in the Dropbox
`covid-19-sharedFiles\data\IDI_raw_outputs\Dwelling_structure`. Note: This data only includes `dwelling_size < 20`
To run this R notebook, copy the `Dwelling_structure` folder to the gitlab repository `complexContagion\Code\Rcode\inputs\DwellingLayer`.
Always make sure to open the `.Rproj` file located in `complexContagion\Code\Rcode` prior to running, as this will make the `here()` library point to the correct directories when loading inputs and writing outputs. 

# libraries
```{R}
library(here)
library(tidyverse)
library(splitstackshape)
```

```{R}
message(paste("The working directory found by the here function is:", here()))
message(paste("All inputs should be put into the folder (and it's subdirectories)", here("inputs")))
message(paste("All outputs should be saved into the folder (and it's subdirectories)", here("outputs")))
```


# load input
```{R}
## these data files should be in Dropbox/data/IDI_raw_outputs/Dwelling_structure
dwellings_sa2 <- read_csv(here("data", "dwelling_composition_V2_sa2_safe - Checked.csv"))
dwellings_ta <- read_csv(here("data", "dwelling_composition_V2_ta_safe - Checked.csv")) %>%
  filter(ur_ta != 999)
dwellings_nz <- read_csv(here("data", "dwelling_composition_V2_nz_safe - Checked.csv"))

```
# load concordance
can be found `Dropbox (Complex systems)\covid-19-sharedFiles\data\spatialdata\concordances`
```{r loading concordance}
# this data needs to be copied from Dropbox/data/spatial and Dropbox/data/IDI_raw_outputs into git-repo/RCode/inputs

concordance_names <-read_csv(here("inputs","SA2_TA18_REGC_concordance.csv"),
                        col_types = cols(
                          SA22018 = col_double(),
                          SA22018_N = col_character(),
                          REGC2018 = col_double(),
                          REGC2018_N = col_character(),
                          TA2018 = col_double(),
                          TA2018_N = col_character(),
                          SA2_area = col_double()
                          )
                        ) %>%
  select(starts_with('SA22018'),starts_with("TA2018")) %>%
  unique()


concordance <- read_csv(here("inputs","MB18_SA2_majority_concordance_IDI.csv"),
                        col_types = cols(
                          MB2018 = col_double(),
                          SA22018 = col_double(),
                          REGC2018 = col_double(),
                          TA2018 = col_double(),
                          DHB2015 = col_double(),
                          DHBCON2015 = col_double()
                        )
                        ) %>%
  select(SA22018,TA2018) %>%
  unique() %>%
  left_join(concordance_names %>% select(SA22018,SA22018_N), by = "SA22018") %>%
  left_join(concordance_names %>% select(TA2018,TA2018_N) %>% unique(), by = "TA2018") %>%
  select(SA22018,SA22018_N,TA2018,TA2018_N)

head(concordance)

```

#Method 1
Method 1 involves simply expanding the rows of the SA2 level table so a full node list is produced. 
The majority of dwellings will have full configuration information, but some will be missing info. 
This may be a strength as it will make the matching process more leniant, and it is easy to do. 
This method does not involve the NZ or TA level data.

```{R}
set.seed(123) #since we will be randomly allocating counts 1-5 where data is missing, set seed.

#Begin by saving cleaned copies of the NZ, TA and SA2 level data. 

dwellings_nz_clean <- dwellings_nz %>% 
  rowwise() %>%
  #if count_dwellings is missing (999999), then fill in with random number from 1 to 5
  mutate(Count_dwellings = ifelse(Count_dwellings == "999999", sample(1:5),Count_dwellings)) %>%
  #get the number of people by multiplying the dwelling size by the count of those dwellings.
  mutate(Count_people = ifelse(Count_people == "999999", parse_number(x =dwelling_size)*Count_dwellings, Count_people)) %>%
  ungroup() 

write_csv(dwellings_nz_clean,here("outputs","dwelling_composition_V2_nz_safe_cleaned.csv"))

dwellings_ta_clean <- dwellings_ta %>% 
  rowwise() %>%
  #if count_dwellings is missing (999999), then fill in with random number from 1 to 5
  mutate(Count_dwellings = ifelse(Count_dwellings == "999999", sample(1:5),Count_dwellings)) %>%
  #get the number of people by multiplying the dwelling size by the count of those dwellings.
  mutate(Count_people = ifelse(Count_people == "999999", parse_number(x =dwelling_size)*Count_dwellings, Count_people)) %>%
  ungroup() %>%
  mutate(TA2018 = str_pad(ur_ta, width = 2, side = "left", pad=0)) %>% 
  filter(TA2018 != 999) %>% select(TA2018,everything(),-ur_ta)

write_csv(dwellings_ta_clean,here("outputs","dwelling_composition_V2_ta_safe_cleaned.csv"))

#NZ Clean
dwellings_sa2_clean <- dwellings_sa2 %>% 
  rowwise() %>%
  #if count_dwellings is missing (999999), then fill in with random number from 1 to 5
  mutate(Count_dwellings = ifelse(Count_dwellings == "999999", sample(1:5),Count_dwellings)) %>%
  #get the number of people by multiplying the dwelling size by the count of those dwellings.
  mutate(Count_people = ifelse(Count_people == "999999", parse_number(x =dwelling_size)*Count_dwellings, Count_people)) %>%
  ungroup() %>%
#join concordance an remove TA 999 
  left_join(concordance %>% select(SA22018,TA2018) %>% unique, by = c("ur_sa2" = "SA22018")) %>%
  mutate(TA2018 = str_pad(TA2018, width = 2, side = "left", pad=0)) %>% 
  filter(TA2018 != 999) %>% select(ur_sa2,TA2018, everything())

write_csv(dwellings_sa2_clean,here("outputs","dwelling_composition_V2_sa2_safe_cleaned.csv"))

```

```{R}
#expand those we already know (Baseline-Step3)
dwellings_sa2_nodelist <- expandRows(dwellings_sa2_clean, count = "Count_dwellings")
```

# fill in step 4 counts
First we need to allocate dwelling sizes to STEP4 aggregation rows using the count of people that should go there.
We'll allocate these people to different sized dwellings according to a truncated lognormal distribution using the function. 
`distribution_allocation`.
```{R}
set.seed(123) #since we will be randomly allocating counts 1-5 where data is missing, set seed.

#This function will take a Count_people (M) and break it into N integers (N being the count of dwellings)
#according to some distribution. log normal seems to fit overall - potentially we can improve here. 
#function found here: https://stackoverflow.com/questions/24845909/generate-n-random-integers-that-sum-to-m-in-r
distribution_allocation <- function(N, M, Lwr, Upr, sd = 1, pos.only = TRUE) {
  #get values from distribution (log norm??)
  #vec <- rnorm(N, M/N, sd) #normal
  #vec <- rlnorm(N, M/N, sd) # log normal
  vec <- EnvStats::rlnormTrunc(N, M/N, sd, min = Lwr,max = Upr) #truncated log normal

  if (abs(sum(vec)) == Inf) vec <- rep(M/N, N)
  #round the values from the distribution
  vec <- round(vec / sum(vec) * M)
  #rounding will throw some values away from the total we're after - call this deviation
  deviation <- M - sum(vec)
  #reallocate the deviation to sample of values so values add up to correct total
  for (. in seq_len(abs(deviation))) {
    vec[i] <- vec[i <- sample(N, 1)] + sign(deviation)
  }
  #If there are more houses to fill than people to fill them, we will get a 0 count regardless
  #to stop code from breaking, we will accomodate this.
  if (N>M) {while (any(vec < 0)) {
    negs <- vec <= 0
    pos  <- vec > 0
    vec[negs][i] <- vec[negs][i <- sample(sum(negs), 1)] + 1
    vec[pos][i]  <- vec[pos ][i <- sample(sum(pos ), 1)] - 1
  }
    #otherwise we will keep shuffling until all vectors are > 0
    }else{
    while (any(vec <= 0)) {
    negs <- vec <= 0
    pos  <- vec > 1
    vec[negs][i] <- vec[negs][i <- sample(sum(negs), 1)] + 1
    vec[pos][i]  <- vec[pos ][i <- sample(sum(pos ), 1)] - 1
    }
      }
  vec
}

dwellings_sa2_nodelist_complete <- dwellings_sa2_nodelist %>%
  group_by(ur_sa2, Step) %>%
  #apply distribution allocation function
  mutate(dwelling_size = ifelse(Step == "Step4", distribution_allocation(N = n(),
                                                                         M = first(Count_people),
                                                                         Lwr = parse_number(first(dwelling_size)),
                                                                         Upr = 19),
                                as.double(dwelling_size)
                                )
         ) %>%
  ungroup() %>%
  #tidy
  rename(SA22018 = "ur_sa2") %>%
  group_by(TA2018) %>%
  #create dwelling ID (these IDs may get recast later on when the network is built)
  mutate(Dwelling_ID = paste0("DWS",
                              str_pad(TA2018,width=2,side="left",pad="0"),
                              str_pad(row_number(), width = 7, side="left", pad = "0"))) %>%
    ungroup() %>%
  select(Dwelling_ID,SA22018, TA2018, everything(),-Count_people)


```

```{R}
write_csv(dwellings_sa2_nodelist_complete, here("outputs", "smallDwellings_sa2_nodelist.csv"))
write_csv(dwellings_sa2_nodelist_complete, here("outputs", paste0(Sys.Date(),"-smallDwellings_sa2_nodelist.csv")))

```
```{R}
dwellings_sa2_nodelist_complete %>% group_by(dwelling_size) %>%
  mutate(freq = n()) %>%
  ggplot(aes(x = dwelling_size, y = freq)) +
  geom_point()


```

# Method 2
Method 2 involves using the full information on dwelling configurations at NZ level to inform TA level, and then SA2 level.
This process will mean more dwelling information is encoded for each dwelling, which may make the matching process more difficult.
This method may be a strength, in that it may provide a dwelling node list that is more accurate.
However, it is more difficult to code and is time-intensive.
## Clean

## What does the dwelling nodelist looklike without geospatial information?
First I'll clean the `dwellings_nz` dataframe as this will have the most complete information. I will need to use this dataframe as a reference later on when determining how many dwellings of each configuration are needed. 
```{R}
# set.seed(123) #since we will be randomly allocating counts 1-5 where data is missing, set seed.
# #NZ Clean
# dwellings_nz_clean <- dwellings_nz %>% 
#   #if count_dwellings is missing (999999), then fill in with random number from 1 to 5
#   mutate(Count_dwellings = ifelse(Count_dwellings == "999999", sample(1:5),Count_dwellings)) %>%
#   #get the number of people by multiplying the dwelling size by the count of those dwellings.
#   mutate(Count_people = parse_number(x = dwelling_size)*Count_dwellings)
# 
#                 
# 
# #Produce nodelist
# dwellings_nz_nodelist <- expandRows(dwellings_nz_clean, count = "Count_dwellings")
# 
# dwellings_nz_nodelist %>%
#   select(Step) %>%
#   mutate(Step = as.factor(Step)) %>%
#   summary()
```

## TA Level
```{R}
# set.seed(123) #since we will be randomly allocating counts 1-5 where data is missing, set seed.
# #NZ Clean
# dwellings_ta_clean <- dwellings_ta %>% 
#   #if count_dwellings is missing (999999), then fill in with random number from 1 to 5
#   mutate(Count_dwellings = ifelse(Count_dwellings == "999999", sample(1:5),Count_dwellings)) %>%
#   #get the number of people by multiplying the dwelling size by the count of those dwellings.
#   mutate(Count_people = parse_number(x = dwelling_size)*Count_dwellings)
# 
# 
# #Produce nodelist
# dwellings_ta_nodelist <- expandRows(dwellings_ta_clean, count = "Count_dwellings")
# 
# dwellings_ta_nodelist %>%
#   select(Step) %>%
#   mutate(Step = as.factor(Step)) %>%
#   summary()
```
## Using NZ to fill in missing TA dwelling configuration

```{R}
# #first get a list of the configuarations that are present in the NZ data, but missing from the TA data.
# list_of_missing_NZ_configs <- dwellings_nz_clean %>% 
#   anti_join(dwellings_ta_clean,
#             by = c("ConfigA_0to14","ConfigB_15to29","ConfigC_30to59","ConfigD_60plus",
#                              "Config_workingAge", "Config_adults", "dwelling_size"))
# 
# 
# #For rows with missing information, fill in information from the difference in the TA data to the NZ data. 
# # for example, if we take the difference in count of dwellings present in the TA data compared to the NZ data, it may show that there are 5 fewer houses with a specific configuration - we need to fill in info to match the NZ counts. We can do this by creating a dataframe of individual dwellings that are missing from the TA dataframe compared to the NZ dataframe, and then sampling without replacement. 
# 
# #First we will grab the NZ dwelling configurations that are not present at all in the TA data.
# list_of_missing_NZ_configs <- dwellings_nz_clean %>% 
#   anti_join(dwellings_ta_clean,
#             by = c("ConfigA_0to14","ConfigB_15to29","ConfigC_30to59","ConfigD_60plus",
#                              "Config_workingAge", "Config_adults", "dwelling_size"))
# 
# #Next find the difference in `count_dwellings` between NZ and TA dwelling configurations present in both datasets.
# dwellings_ta_nz_diff <- dwellings_ta_clean %>%
#   #remove TA info
#   select(-ur_ta, -Count_people) %>% 
#   group_by(ConfigA_0to14,ConfigB_15to29,ConfigC_30to59,ConfigD_60plus,
#            Config_workingAge, Config_adults, dwelling_size,Step) %>%
#   summarise(Count_dwellings_TA = sum(Count_dwellings)) %>%
#   ungroup() %>% 
#   #Join to NZ data
#   inner_join(dwellings_nz_clean, by = c("ConfigA_0to14","ConfigB_15to29","ConfigC_30to59","ConfigD_60plus",
#                              "Config_workingAge", "Config_adults", "dwelling_size", "Step")) %>%
#   mutate(NZ_TA_Diff = Count_dwellings - Count_dwellings_TA)
#   
# #check differences
# # negative differences should be due to random rounding, positive differences due to loss of info at Ta level with levels of aggregation. 
# # extreme negative values should be due to aggregation steps not present in NZ data 
# dwellings_ta_nz_diff %>% 
#   mutate(row = row_number()) %>%
#   ggplot(aes(x = row, y = NZ_TA_Diff, colour = Step)) + 
#   geom_point(stat = "identity")
# 
# 
# #now create the table of configurations that we can sample from to fill in missing info. 
# pool_of_missing_NZ_configs <- dwellings_ta_nz_diff %>%
#   filter(NZ_TA_Diff > 0) %>% 
#   #remove columns not needed
#   select(-Count_dwellings_TA, -Count_dwellings, -Count_people) %>%
#   #expand counts to individual rows
#   expandRows(count = "NZ_TA_Diff") %>%
#   #Create a column that refers to the dwelling configuration, this will make it easier to check if it is present in the TA.
#   mutate(config_code = paste0(str_pad(ConfigA_0to14, width = 2, side = "left", pad = "0"), # Pad components of the string to make it consistent
#                               str_pad(ConfigB_15to29, width = 2, side = "left", pad = "0"),
#                               str_pad(ConfigC_30to59, width = 2, side = "left", pad = "0"),
#                               str_pad(ConfigD_60plus, width = 2, side = "left", pad = "0"),
#                               str_pad(Config_workingAge, width = 2, side = "left", pad = "0"),
#                               str_pad(Config_adults, width = 2, side = "left", pad = "0"),
#                               str_pad(dwelling_size, width = 2, side = "left", pad = "0")
#                               )
#          )
# 
# #Add the same config code to TA level data
# #Create a column that refers to the dwelling configuration, this will make it easier to check if it is present in the TA.
# dwellings_ta_nodelist<- dwellings_ta_nodelist %>%
#   # Pad components of the string to make it consistent
#   mutate(config_code = paste0(str_pad(ConfigA_0to14, width = 2, side = "left", pad = "0"), 
#                               str_pad(ConfigB_15to29, width = 2, side = "left", pad = "0"),
#                               str_pad(ConfigC_30to59, width = 2, side = "left", pad = "0"),
#                               str_pad(ConfigD_60plus, width = 2, side = "left", pad = "0"),
#                               str_pad(Config_workingAge, width = 2, side = "left", pad = "0"),
#                               str_pad(Config_adults, width = 2, side = "left", pad = "0"),
#                               str_pad(dwelling_size, width = 2, side = "left", pad = "0")
#                               )
#        )
# 
# summary(dwellings_ta_nodelist)
```


Now we have the dataframe of individual missing dwelling configurations, we can go through each TA and fill in missing info. 
We can narrow the matching process by specifying that we only sample a configuration that is not already present in that TA.
We will begin with data missing at Step1 level, then pass that dataframe to Steps2-4
### Fill in at Step 1
```{R}
# # create copy of pool_of_missing_NZ_configs so we don't have to rerun previous chunk.
# pool_of_missing_NZ_configs_operational <- pool_of_missing_NZ_configs
# 
# 
# #Create df to store filled data
# dwellings_ta_filled_Step1 <- data.frame() 
# 
# #for specific TA
# for(TA in unique(dwellings_ta_nodelist$ur_ta)){
# dwellings_ta_group <- dwellings_ta_nodelist %>% filter(ur_ta == TA) 
# #get list of current configurations already within that TA
# present_TA_config_codes <- unique(dwellings_ta_group$config_code)
# #For each row with missing data, fill in config info from pool_of_missing_NZ_configs
# for(i in 1:nrow(dwellings_ta_group)){
# 
#   #fill step 1 info first
#   if(dwellings_ta_group[i,"Step"]=="Step1"){
#     #if row i has missing info in ConfigB_15to29 ConfigC_30to59 (i.e., Step 1 aggregation) then we want to fill it.
#     #find the rows in pool_of_missing_NZ_configs that contain suitable replacements (rest of info matches)
#     candidates <- pool_of_missing_NZ_configs_operational %>% 
#       #get configurations that match available data
#       filter(ConfigA_0to14 == dwellings_ta_group[i, "ConfigA_0to14"][[1]],
#              ConfigD_60plus == dwellings_ta_group[i, "ConfigD_60plus"][[1]],
#              Config_workingAge == dwellings_ta_group[i, "Config_workingAge"][[1]],
#              Config_adults == dwellings_ta_group[i, "Config_adults"][[1]],
#              dwelling_size == dwellings_ta_group[i, "dwelling_size"][[1]]) %>%
#       #remove candidates that have a configuration that is already present in the TA
#       filter(!(config_code %in% present_TA_config_codes))
#     #select a candidate if there is one
#     if(nrow(candidates)>0){
#     #randomly select 1 row
#     selected_candidate <- candidates[sample(nrow(candidates), size = 1),]
#     # and fill in the info for i using that selected candidate
#     filled_row <- dwellings_ta_group[i,]
#     filled_row$ConfigB_15to29 <- selected_candidate$ConfigB_15to29
#     filled_row$ConfigC_30to59 <- selected_candidate$ConfigC_30to59
# 
#     #finally remove that candidate from the pool so it can't be used again
#     #get the index location of the match
#     candidate_location <- match(selected_candidate$config_code[[1]], pool_of_missing_NZ_configs_operational$config_code)
#     #then remove
#     pool_of_missing_NZ_configs_operational <- pool_of_missing_NZ_configs_operational[-candidate_location,]
#     }
#     
#   }else{filled_row <- dwellings_ta_group[i,]}
#   dwellings_ta_filled_Step1 <- rbind(dwellings_ta_filled_Step1, filled_row)
# }
# print(TA)
# }
# summary(dwellings_ta_filled_Step1)
```

### Step 2
```{R}
# #Create df to store filled data
# dwellings_ta_filled_Step2 <- data.frame()
# 
# 
# #for specific TA
# for(TA in unique(dwellings_ta_filled_Step1$ur_ta)){
# dwellings_ta_filled_Step1_group <- dwellings_ta_filled_Step1 %>% filter(ur_ta == TA) 
# #get list of current configurations already within that TA
# present_TA_config_codes <- unique(dwellings_ta_group$config_code)
# #For each row with missing data, fill in config info from pool_of_missing_NZ_configs
# for(i in 1:nrow(dwellings_ta_filled_Step1_group)){
# 
#   #fill step 1 info first
#   if(dwellings_ta_filled_Step1_group[i,"Step"]=="Step2"){
#     #if row i only has adult and child info (i.e., Step 2 aggregation) then we want to fill in the rest of the info.
#     #find the rows in pool_of_missing_NZ_configs that contain suitable replacements (rest of info matches)
#     candidates <- pool_of_missing_NZ_configs_operational %>% 
#       #get configurations that match available data
#       filter(ConfigA_0to14 == dwellings_ta_filled_Step1_group[i, "ConfigA_0to14"][[1]],
#              Config_adults == dwellings_ta_filled_Step1_group[i, "Config_adults"][[1]],
#              dwelling_size == dwellings_ta_filled_Step1_group[i, "dwelling_size"][[1]]) %>%
#       #remove candidates that have a configuration that is already present in the TA
#       filter(!(config_code %in% present_TA_config_codes))
#     #select a candidate if there is one
#     if(nrow(candidates)>0){
#     #randomly select 1 row
#     selected_candidate <- candidates[sample(nrow(candidates), size = 1),]
#     # and fill in the info for i using that selected candidate
#     filled_row <- dwellings_ta_filled_Step1_group[i,]
#     filled_row$ConfigB_15to29 <- selected_candidate$ConfigB_15to29
#     filled_row$ConfigC_30to59 <- selected_candidate$ConfigC_30to59
#     filled_row$ConfigD_60plus <- selected_candidate$ConfigD_60plus
#     filled_row$Config_workingAge <- selected_candidate$Config_workingAge
# 
# 
#     #finally remove that candidate from the pool so it can't be used again
#     #get the index location of the match
#     candidate_location <- match(selected_candidate$config_code[[1]], pool_of_missing_NZ_configs_operational$config_code)
#     #then remove
#     pool_of_missing_NZ_configs_operational <- pool_of_missing_NZ_configs_operational[-candidate_location,]
#     }
#     
#   }else{filled_row <- dwellings_ta_filled_Step1_group[i,]}
#   dwellings_ta_filled_Step2 <- rbind(dwellings_ta_filled_Step2, filled_row)
# }
# print(TA)
# }
# summary(dwellings_ta_filled_Step2)
```

### Step 3
```{R}
# #Create df to store filled data
# dwellings_ta_filled_Step3 <- data.frame()
# for(TA in unique(dwellings_ta_filled_Step2$ur_ta)){
# #for specific TA
# dwellings_ta_filled_Step2_group <- dwellings_ta_filled_Step2 %>% filter(ur_ta == TA) 
# #get list of current configurations already within that TA
# present_TA_config_codes <- unique(dwellings_ta_group$config_code)
# #For each row with missing data, fill in config info from pool_of_missing_NZ_configs
# for(i in 1:nrow(dwellings_ta_filled_Step2_group)){
#   #fill step info
#   if(dwellings_ta_filled_Step2_group[i,"Step"]=="Step2"){
#     #if row i only has dwelling size info (i.e., Step 3 aggregation) then we want to fill in the configuration info
#     #find the rows in pool_of_missing_NZ_configs that contain suitable replacements (rest of info matches)
#     candidates <- pool_of_missing_NZ_configs_operational %>% 
#       #get configurations that match available data
#       filter(dwelling_size == dwellings_ta_filled_Step1_group[i, "dwelling_size"][[1]]) %>%
#       #remove candidates that have a configuration that is already present in the TA
#       filter(!(config_code %in% present_TA_config_codes))
#     #select a candidate if there is one
#     if(nrow(candidates)>0){
#     #randomly select 1 row
#     selected_candidate <- candidates[sample(nrow(candidates), size = 1),]
#     # and fill in the info for i using that selected candidate
#     filled_row <- dwellings_ta_filled_Step2_group[i,]
#     filled_row$ConfigA_0to14 <- selected_candidate$ConfigA_0to14
#     filled_row$ConfigB_15to29 <- selected_candidate$ConfigB_15to29
#     filled_row$ConfigC_30to59 <- selected_candidate$ConfigC_30to59
#     filled_row$ConfigD_60plus <- selected_candidate$ConfigD_60plus
#     filled_row$Config_workingAge <- selected_candidate$Config_workingAge
#     filled_row$Config_adults <- selected_candidate$Config_adults
# 
#     #finally remove that candidate from the pool so it can't be used again
#     #get the index location of the match
#     candidate_location <- match(selected_candidate$config_code[[1]], pool_of_missing_NZ_configs_operational$config_code)
#     #then remove
#     pool_of_missing_NZ_configs_operational <- pool_of_missing_NZ_configs_operational[-candidate_location,]
#     }
#     
#   }else{filled_row <- dwellings_ta_filled_Step2_group[i,]}
#   dwellings_ta_filled_Step3 <- rbind(dwellings_ta_filled_Step3, filled_row)
# }
# print(TA)
# }
# 
# summary(dwellings_ta_filled_Step3)
```


## SA2 Level
```{R}
# set.seed(123) #since we will be randomly allocating counts 1-5 where data is missing, set seed.
# #NZ Clean
# dwellings_sa2_clean <- dwellings_sa2 %>% 
#   #if count_dwellings is missing (999999), then fill in with random number from 1 to 5
#   mutate(Count_dwellings = ifelse(Count_dwellings == "999999", sample(1:5),Count_dwellings)) %>%
#   #get the number of people by multiplying the dwelling size by the count of those dwellings.
#   mutate(Count_people = parse_number(x = dwelling_size)*Count_dwellings)
# 
# dwellings_sa2_clean %>% filter(Step == "Step4")
# #Produce nodelist
# dwellings_sa2_nodelist <- expandRows(dwellings_sa2_clean, count = "Count_dwellings") %>% 
#   left_join(concordance %>% select(SA22018,TA2018) %>% unique, by = c("ur_sa2" = "SA22018")) %>%
#   mutate(TA2018 = str_pad(TA2018, width = 2, side = "left", pad=0)) %>%
#   group_by(TA2018) %>%
#   mutate(Dwelling_ID = paste0("DWS",TA2018,str_pad(row_number(), width = 7, side="left", pad = "0"))) %>%
#   select(Dwelling_ID, TA2018, ur_sa2, everything()) %>%
#   ungroup()
# 
# 
# dwellings_sa2_nodelist %>%
#   select(Step) %>%
#   mutate(Step = as.factor(Step)) %>%
#   summary()
```